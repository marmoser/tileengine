({
  baseURL: 'res',
  out: 'build/tile_engine.js',
  include: 'res/bootstrap.js',
  optimize: 'none',
  paths: {
    "jquery": "empty:",
    "jquery-ui": "empty:"
  }
})
