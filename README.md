# Html 5 game engine for a tile-based tactical browser game.

## Features:
 * Turn-based gameplay: play with your team against the enemy (AI-controlled) team.
 * Renders isometric maps that allow zooming and navigation
 * Encourages modification (graphics are isometric spritesheets, maps are json files)
 * Comes with an interactive editor that allows users to create their own maps by editing ground tiles and object tiles

## Gameplay:
 * Drag and drop inventory system
 * Shadow overlays
 * Radial menus for performing interactions
 * Point-based movement/action system
 * Basic AI for the enemy team that performs decision making and carries out actions

Current state: playable (with bugs)

## Keybindings:
  * Cursor keys: scroll map
  * left mouse button: select actor
  * right mouse button: open radial menu
  * 'i': open inventory
  * 'a': zoom in, 's': zoom out
  * 'u': show visible tiles for each actor

## Dependencies
  require.js
  jquery (should be removed in a future version)
  jquery-ui (should be removed in a future version)

## How to build:
  Makefile (uses r.js and babel-minify) to build a single js file

Pathfinding and sprite loading are adapted from  http://www.jsiso.com/

## Screenshots
Gameplay:
![picture](https://bitbucket.org/marmoser/tileengine/raw/0ca1fafece6e5ac93239646fef210eefe129fa50/screenshots/gameplay.png)

Editor:
![picture](https://bitbucket.org/marmoser/tileengine/raw/3a82da03cb0477d3e460817acf0fd9ae61047f14/screenshots/editor.png)

## License
MIT



