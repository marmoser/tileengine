/*
  GroundItemBatch.js
  Container object for items positioned/dropped on the ground
  allows positioning of items on tiles
  
  functions:
  -open inventory with contained items
  -destroy if emptied
  -show items if visible
  
  todo: implement traps
*/

define(
function() {

  //constructor
  function GroundItemBatch(items) {
    //this container cannot be empty and should be destroyed when emptied
    if (!typeof items === 'object') {
      throw new TypeError("items cannot be empty");
    }
  
    this.inventory = new Array();
    this.class = "GroundItems";
    this.discovered = true;

    if (Array.isArray(items)) {
      for (var i=0;i<items.length;i++) {
        if (typeof(items[i].obj) == 'object') {
          //we have an inventory array as argument and use its objects without slots
          this.inventory.push({obj: items[i].obj});
        } else {
          this.inventory.push({obj: window.TileEngine.itemlist[items[i].index].spawnNew() });
        }
      }
    } else {
       if (items.constructor.name == 'ItemInstance') {
         this.inventory.push({obj: items});
       }
    }
  }

  GroundItemBatch.prototype = {
    constructor: GroundItemBatch,
    getRadialActions: function(actor) {
      return {
        'pickup': 'open'
      }
    },
    open: function(a) {
      var pos = window.TileEngine.getObjLayout().getTilePos(a.actor.currentX+4,a.actor.currentY+4);
      window.TileEngine.drawInventoryPopover(this,pos.x,pos.y);
      a.actor.openCharacterInventory(a.actor.currentX,a.actor.currentY);
    },
    drawImage: function(tileX,tileY) {
      if (tileX && tileY) {
        window.TileEngine.tileField[0].drawShadow(tileX,tileY,'rgba(255, 255, 0, 0.2)');
      }
      //idea: draw items directly
      if (this.inventory.length > 0) {
        var img = this.inventory[0].obj.blueprint.image;
        //scale to fit std dimensions!
        img.width= 100;
        img.height= 50;
        return img;
      }
      return "1-barrel1.png"
    },
    addItem: function(item) {
        this.inventory.push({obj: item});
    },
    closeFunction: function() {
      console.debug('close function called');
      if (this.inventory.length === 0) {
        //mark for deletion
        this.destroyed = 1;
      }
    },
    EditorProperties: function() {
     return [
        {"name":"condition","type":"integer","value":100}
     ]
    }
  }
  return GroundItemBatch;
});
