requirejs.config( {
  paths: {
    "jquery": "libs/jquery-1.12.4",
    "jquery-ui": "libs/jquery-ui.min"
  }
} );

var MainClass = document.getElementById('bootstrap_script').getAttribute('data-application');
if (MainClass == '') {
  //load both for the build
  require(['res/TacticalMain', 'res/EditorMain']);
}

require([
  'res/'+MainClass,
  'jquery',
  'jquery-ui',
],
        function(TacticalMain) {
          window.tactical = new TacticalMain();
          //tactical main starts the tactical simulation
          window.tactical.bootstrap("json/map.json");
          window.MainClass = MainClass;

          $( window ).resize( function () {
            window.tactical.updateDimensions(window.innerWidth, window.innerHeight);
          });
        });
