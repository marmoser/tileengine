/*
  tactical engine
  draw tiles, setup everything
*/
define([
      '../res/TileEngine',
      '../res/ContentLoader',
      '../res/InputHandler',
      '../res/Actor'
    ],
    function(TileEngine, ContentLoader, InputHandler, Actor) {
    return function() {
      //helper for window redraw
      window.requestAnimFrame = (function() {
        return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame  ||
        window.mozRequestAnimationFrame     ||
        window.oRequestAnimationFrame       ||
        window.msRequestAnimationFrame      ||
        function(callback, element) {
          window.setTimeout(callback, 1000 / 60);
        };
      })();

      //init main stuff
      function _launch() {
        //hacky: disable contextmenu
        $(document).on("contextmenu", function() {return false;});
        var tilelist = [];
        var xrange = 20;
        var yrange = 20;
        this.contentLoader = new ContentLoader();
        this.contentLoader.initGameData("json/map.json").then(function(r) {
          window.TileEngine = new TileEngine(window.innerWidth*0.65, window.innerHeight, r);
          window.TileEngine.initLayers(r);
          window.TileEngine.range = {x:xrange,y:yrange};
          window.TileEngine.centerView(xrange,yrange);
          var input = new InputHandler(document, window.TileEngine.context, 'editor');
          registerEditorFuctions(r);
          window.TileEngine.render();
        });
      }

      function registerEditorFuctions(r) {
        window.objectMode = false;
        fillSelector(window.TileEngine.layers[0]);
        $("#navbar_header").append(
          //export current map to json
          $("<a>", {href: "#", html: 'Export/Save&nbsp;', target: '_blank' }).click(function(e) {
            $(this).attr('href', window.TileEngine.exportCurrentMap());
            return false;
          })
        ).append(
          $("<a>", {href: "#", html: 'New map', target: '_blank'}).click(function(e) {
            var emptyarray = [];
            var emptyarray2 = [];
            for (var i = 0; i <= window.TileEngine.getMapSize(); i++) {
              emptyarray[i] = new Array(window.TileEngine.getMapSize());
              emptyarray2[i] = new Array(window.TileEngine.getMapSize());
              for (var j = 0; j<=window.TileEngine.getMapSize(); j++) {
                emptyarray[i][j] = 0;
                emptyarray2[i][j] = 0;
              }
            }
            //load an empty map
            window.TileEngine.initLayers({
              "groundLayerDefinition": emptyarray,
              "structureLayerDefinition": emptyarray2,
              "actorDefinitions": ""});
            return false;
          })
        ).append(
          $("<button>", {type: "radio", html:"View: Toggle Object Layer"})
            .click(function(e) {
              window.TileEngine.hideObjects ^= 1;
            }).css("float", "right")
        ).append(
          $("<button>", {type: "radio", html:"Show ground/object sprites"})
            .click(function(e) {
              window.objectMode ^= 1;
              window.actorMode = 0;
              if (window.objectMode) {
                fillSelector(window.TileEngine.layers[1]);
              } else {
                fillSelector(window.TileEngine.layers[0]);
              }
            }).css("float", "right")
        ).append(
          $("<button>", {type: "radio", html:"Show Interactive Objects"})
            .click(function(e) {
              $("#elementselector").empty();
              $("#elementdetails_form").empty();
              window.actorMode ^= 1;
              //actor sprite
              var asprites = window.TileEngine.gamedata.actorSprites;
              var img = asprites.files[asprites.dictionary["front1"]];
              $(img).click(function(e){
                window.selectedTile = 'Actor';
              });
              $("#elementselector").append(img);
              var objectlayer = window.TileEngine.layers[1];
              var objectsprites = window.TileEngine.gamedata.objectImagesLoaded;
              for (var i in objectlayer.objectProperties) {
                if (objectlayer.objectProperties[i].type == 'door' ||
                   objectlayer.objectProperties[i].type == 'container') {
                  var imgD = objectsprites.files[i];
                  $(imgD).data("x",i);
                  $(imgD).click(function(e){
                    var j = $(this).data();
                    var objectlayer = window.TileEngine.layers[1];
                    window.selectedTile = objectlayer.objectProperties[j.x];
                  });
                  $("#elementselector").append(imgD);
                }
              }
            }).css("float", "right")
        );
      }
      function _updateDimensions(w, h) {
        var canvas = window.TileEngine.context.canvas;
        canvas.width = w*0.65;
        canvas.height = h*0.8;
      }
      function fillSelector (layer) {
        //fill elementselector
        var ground_dict = layer.graphicsDictionary;
        var ground_graphics = layer.graphics;
        $("#elementdetails_form").empty();
        $("#elementselector").empty();
        for (var i=0;i<ground_dict.length; i++) {
          //skip doors and tiles without object definition
          if (layer.title == 'objects' &&
              (typeof layer.objectProperties[i] == 'undefined' ||
               layer.objectProperties[i].type == 'door')) continue;
          var img = ground_graphics[ground_dict[i]];
          if (i == 0) {
            //default selection after loading
            $(img).css("background","green");
            window.selectedTile = {x: 0};
          }
          //write dictionary value to data
          $(img).data("x",i);
          $(img).click(function(e){
            $(this).siblings().css("background", "none");
            $(this).css("background","green");
            window.selectedTile = $(this).data();
            if (layer.objectProperties) {
              var html = "";
              var tileProperties  = layer.objectProperties[window.selectedTile.x];
              for (var key in tileProperties) {
                html+= key+': '+tileProperties[key]+'<br>';
              }
              $("#elementdetails_form").empty();
              $("#elementdescriptor").html(html);
            }
          });
          $("#elementselector").append(img);
        }
      }
      function _propertiesToHtml (plist) {
        var out = "";
        for (p of plist) {
          if (p.type == 'range') {
            if(!p.step) {p.step = 1}
            if(!p.min) {p.min = 0}
            out+="<p>"+p.name+"\
<input data-type='integer' name='"+p.name+"' type='range' value="+p.value+" \
min='"+p.min+"' max="+p.max+" step='"+p.step+"'oninput='this.nextElementSibling.value = this.value'>\
<input data-type='integer' type='text' class='output' value="+p.value+"></p>";
          } else {
            out+="<p>"+p.name+"<input data-type='"+p.type+"' name='"+p.name+"' type='text' value="+p.value+"></p>";
          }
        }
        return out;
      }

      return {
        //wrappers for internal functions
        bootstrap: _launch,
        updateDimensions: _updateDimensions,
        propertiesToHtml: _propertiesToHtml
      };

    }}
);
