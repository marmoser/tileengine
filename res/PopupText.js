/*
  PopupText.js
  Render a text over the canvas
*/

define(
function() {
  //constructor
  function PopupText(context, text, position, color, size, duration) {
    /*
    @context: 2d canvas object for drawing
    @text: string to display
    @position{x,y}: on which tile should we render this?
    @color: specify text color
    @size: font size in px
    @duration: how many draw iterations should this be visible?
    */
    this.context = context;
    this.text = text;
    if (!position) {
      //if no position is passed, center text in the canvas
      position = {x: window.TileEngine.context.canvas.width/2, y: window.TileEngine.context.canvas.height/2};
    }
    this.position = position;
    if (typeof color == 'undefined') {
      this.color = "white";
    } else {
      this.color = color;
    }
    
    if (typeof size == 'undefined') {
      this.size = 10;
    } else {
      this.size = size;
    }
    this.speed = 15;
    if (typeof duration == 'undefined') {
      this.duration = 50;
    } else {
      this.duration = duration;
    }
    this.nrofredraws = 0;
  }

  PopupText.prototype = {
    draw: function() {
      var ctx = this.context;
      ctx.font = this.size+'px Arial';
      ctx.fillStyle = this.color;
      ctx.fillText(this.text, this.position.x, this.position.y);
      if (this.duration > 0) {
        this.position.y-= this.speed*0.2;
        this.nrofredraws++;
      }
      if (this.nrofredraws > this.duration ) {
        this.removeText();
      }
    },
    removeText: function() {
      var index = window.TileEngine.activePopupTexts.indexOf(this);
      if (index > -1) window.TileEngine.activePopupTexts.splice(index,1);
    }
  }
  return PopupText;
});