/*
  do actual rendering by canvas manipulation
  todo: light, shadow, effects
*/
define(function() {
  function Layer(ctx, w, h, settings) {
    this.ctx = ctx;
    this.settings = settings;
    this.setup(settings);
  }

Layer.prototype = {
  constructor: Layer,
  //setup basic vars
  setup: function(settings) {
    this.tileWidth = 100;
    this.tileHeight = 50;
    this.curZoom = 1;
    this.mapLayout = Object.create(settings.layout);
    this.graphics = settings.graphics;
    this.graphicsDictionary = settings.graphicsDictionary;
    this.ignoreZeros = settings.zeroIsBlank;
    this.title = settings.title;
  },
  align: function(xstart, ystart) {
    var offset = 0;
    this.drawY = 0 - (ystart * (this.tileHeight/2))*this.curZoom - (xstart * (this.tileWidth/4))*this.curZoom;
    this.drawX = (window.TileEngine.context.canvas.width / 2) + (ystart * this.tileHeight)*this.curZoom - (xstart * (this.tileWidth/2))*this.curZoom;
    //console.log('xrange '+xrange+' yrange '+yrange);
    //console.log('drawx '+this.drawX+' drawy '+this.drawY);
  },
  //draw a tile
  draw: function(x,y,img) {
    if (!Number.isInteger(x) || !Number.isInteger(y)) { 
        console.log("not a number");
        return;
    }
    if (x < 0 || y < 0) {return;}
    var xpos = (x - y) * (this.tileHeight * this.curZoom) + this.drawX;
    var ypos = (x + y) * (this.tileWidth / 4 * this.curZoom) +this.drawY;
    //todo check if we are outside the defined nr of tiles
    if (img) {
      var stackGraphic = img;
    } else {
      var graphicValue = this.mapLayout[x][y];
      if (this.ignoreZeros) {
        //for now, 0s are ignored, start counting from 1
        if (Number(graphicValue) == 0) {
          return;
        } else {
          graphicValue--;
        }
      }
      if (Number(graphicValue) >= 0) {
        var stackGraphic = this.graphics[this.graphicsDictionary[graphicValue]];
      } else {
        return;
      }
    }
    var resizedTileHeight =  stackGraphic.height / (stackGraphic.width / this.tileWidth);
    this.ctx.drawImage(stackGraphic,
                       0,
                       0,
                       stackGraphic.width,
                       stackGraphic.height,
                       xpos,
                       (ypos + ((this.tileHeight - resizedTileHeight) * this.curZoom)),
                       (this.tileWidth * this.curZoom), (resizedTileHeight * this.curZoom));
  },
  drawShadow: function(x, y, rgba) {
    //draw an rgba overlay over a tile
    //used for creating shadows
    //alpha between 0.0 and 0.7
    this.ctx.fillStyle = rgba;
    this.ctx.beginPath();
    var rectx = (x - y) * (this.tileHeight * this.curZoom) + this.drawX -2;
    var recty = (x + y) * (this.tileWidth / 4 * this.curZoom) +this.drawY -2;
    //draw a rectangle over the tile
    var size= this.tileHeight;
    this.ctx.moveTo(rectx + size*this.curZoom,
                    recty);

    this.ctx.lineTo(rectx + (size + size)*this.curZoom,
                    recty + (size/2)*this.curZoom );

    this.ctx.lineTo(rectx + size*this.curZoom,
                    recty + size*this.curZoom );

    this.ctx.lineTo(rectx,
                    recty + (size/2)*this.curZoom);
    this.ctx.fill();
  },
  getTilePos:function(x, y) {
    //get pixels for map coordinates
    var xpos, ypos;
    xpos = (x - y) * (this.tileHeight * this.curZoom) + this.drawX + this.tileHeight/2 +10;
    ypos = (x + y) * (this.tileWidth / 4 * this.curZoom) + this.drawY + this.tileWidth /4 - 10;
    return {x: xpos, y: ypos};
  },
  getXYCoords:function(x,y) {
    //determine map coords for pixels
    var positionX, positionY;
    positionY = (2 * (y - this.drawY) - x + this.drawX) / 2;
    positionX = x + positionY - this.drawX - (this.tileHeight * this.curZoom);
    positionY = Math.round(positionY / (this.tileHeight * this.curZoom));
    positionX = Math.round(positionX / (this.tileHeight * this.curZoom));
    if (positionX <0) {
      positionX = 0;
    }
    if (positionY <0) {
      positionY = 0;
    }
    return({x: positionX, y: positionY});
  },
  move: function(direction, distance) {
    // left || right || up || down
    var particle, subPart;

    //distance = distance || this.tileHeight;
    distance = 500;
    if (direction === "left") {
      this.drawX += distance * this.curZoom;
    }
    else if (direction === "down") {
      this.drawY += distance / 2 * this.curZoom;
    }
    else if (direction === "up") {
      this.drawY -= distance / 2 * this.curZoom;
    }
    else if (direction === "right") {
      this.drawX -= distance * this.curZoom;
    }
  },
  setZoom: function(dir) {
    this.curZoom = (dir == "in") ? this.curZoom-=0.1 : this.curZoom+=0.1 ;
  }
}
return Layer;
});
