/*
Tactical AI
determine AI actions for this faction
*/
define([
  '../res/Actor',
  '../res/Points',
  '../res/Physics'
], function(Actor, Points, Physics) {
  /*
      constructor
  */
  function AI(name) {
    //faction should be an object linking an AI instance
    //faction {name: 'suckers', AI: obj}
    
    if (!(this instanceof AI)) {
      throw new TypeError("constructor cannot be called as a function.");
    }
    this.name = name;
    this.visibleObjects = {"actors": [], "objects": []};
    this.strategy = {"type": "random", "location": []};
  }

  var Action = {
    create: function(p, ac, loc, tar, obj) {
      var newObj = Object.create(this);
      newObj.p = p;
      newObj.action = ac;
      newObj.location = loc;
      newObj.target = tar;
      newObj.obj = obj;
      //console.debug(newObj);
      return newObj;
    }
  }

  AI.prototype = {
    //repoint the constructor
    constructor: AI,

    decideAction: function(a) {
      console.log('will decide action for' +a.id+'  ap' +a.currentAP);
      //what should Actor do with his AP?
      if (a.currentAP <= 0) {
        a.AIStack.action ="AI_NOACTION";
        return
      }
      if (a.stance == 3) {
        a.AIStack.action = "AI_STAND";
        return
      }
      //what can we see?
      var lastvisibleobjects = Object.create(a.getVisibleEnemies());
      Physics.getVisibleObjects(a);
      //console.log('i see '+a.getVisibleEnemies().length+' i saw '+lastvisibleobjects.length);
      if (a.visibleObjects.actors.length ==0) {
        //move to last target, if we had one and its still alive
        if (lastvisibleobjects.length>0) {
          //move towards last target
          console.log('move to last target');
          var target = lastvisibleobjects[0];
          var rx = target.lastX;
          var ry = target.lastY;
          //todo: how far can we move towards target?
        } else if (this.strategy.type === 'attack') {
          console.log(a.id+' join the attack');
          var rx = this.strategy.location.x;
          var ry = this.strategy.location.y;
        } else {
          //random walking
          console.log('random walk');
          var rx = Math.floor(Math.random() * (30 - 2) + 2);
          var ry = Math.floor(Math.random() * (30 - 2) + 2);
        }
        var loc = Physics.findClosestLocation(a.currentX,a.currentY,rx,ry);
        console.debug('closest loc to '+rx+' '+ry+'=='+loc.x+' '+loc.y);
        a.AIStack.actionLocation = loc;
        a.AIStack.action = "AI_MOVE";
      } else {
        //calc best attack
        var c = Physics.findBestCover(a, 5, a.visibleObjects.actors[0]);
        var rangedattack = this.calcBestRangedAttack(a);
        var hitattack = this.calcBestMeleeAttack(a);
        //decide for one action and schedule it
        if (rangedattack.p > hitattack.p) {
          var preferred = rangedattack;
        } else {
          var preferred = hitattack;
        }
        a.equipItem(preferred.obj);
        if (preferred.p > 0) {
          a.AIStack.action = preferred.action;
          a.AIStack.actionTarget = preferred.target;
          a.AIStack.actionLocation = preferred.location;
        }
      }
    },
    executeAction: function(a) {
      console.log('execute action'+a.AIStack.action);
      if (a.AIStack.action === null || a.AIStack.action === '') {return}
      //adds action to action stack
      var slotitem = window.TileEngine.findForInventorySlot(a.inventory, 'slot_lhand');
      switch (a.AIStack.action) {
      case 'AI_FIRE':
        a.actionqueue.push({'actor': a,
                            'action': 'attack',
                            'target':a.AIStack.actionTarget,
                            'object':slotitem});
        break;
      case 'AI_MELEE':
        var closest_pos = Physics.findClosestLocation(
          a.currentX, a.currentY, a.AIStack.actionTarget.currentX,a.AIStack.actionTarget.currentY);
        if (closest_pos) {
          if (a.currentX != closest_pos.x || a.currentY != closest_pos.y) {
            a.actionqueue.push({'actor': a, 
                                'action': 'moveto', 
                                'x': closest_pos.x, 
                                'y': closest_pos.y});
          }
          //and now punch
          a.actionqueue.push({'actor': a, 'action': 'attack', 'target':a.AIStack.actionTarget});
        }
        break;
      case 'AI_FLEE':
        a.actionqueue.push({'actor': a, 
                            'action': 'moveto', 
                            'x': 32-a.AIStack.actionTarget.currentX, 
                            'y': 32-a.AIStack.actionTarget.currentY});
        break;
      case 'AI_MOVE':
        a.actionqueue.push({'actor': a, 
                            'action': 'moveto', 
                            'x': a.AIStack.actionLocation.x, 
                            'y': a.AIStack.actionLocation.y});
        break;
      case 'AI_MOVE_FIRE':
        a.actionqueue.push({'actor': a,
                            'action': 'moveto',
                            'x': a.AIStack.actionLocation.x,
                            'y': a.AIStack.actionLocation.y});
        a.actionqueue.push({'actor': a, 'action': 'attack', 'target':a.AIStack.actionTarget, 'object':slotitem});
        break;
      case 'AI_STAND':
        a.actionqueue.push({'actor':a, 'action':'toggleStand'});
        break;
      }
      a.performNextAction();
      a.AIStack = {};
    },
    calcBestRangedAttack: function(a) {
      //choose target for ranged attack
      //1)do we have a useful weapon for a ranged attack? if there are several, choose the best
      var in_hand = a.hasInventoryItem('weapon');
      if (!in_hand) {
        return Action.create(0);
      }
      //2)is the item ready to be used? might be empty or broken

      if (typeof in_hand == 'undefined') {
        return Action.create(0);
      }
      if (in_hand.getClipsize() == 0) {
        //reload if necessary
        var ammo = a.hasInventoryItem('ammo');
        if (!ammo || ammo.clipsize == 0) return Action.create(0)
        a.actionqueue.push({'actor': a, 'action': 'reload', 'item': in_hand, 'ammo': ammo});
      }
     
      //3)for each visible actor calculate a chance to hit, choose target with the best chance
      var bestshot = 0;
      var selected_actor = null;
      for (row of a.getVisibleEnemies()) {
        var points = Points.getAPsToAttack(a, row.actor, in_hand);
        var dist = Physics.calculateDistance(a.currentX, a.currentY, row.actor.currentX, row.actor.currentY);
        var range = in_hand.gunrange;
        var moveto = {};
        if (dist > range) {
          //can we walk up to this guy and fire a shot?
          var diff = dist-range+1;
          var ap_needed = diff*Points.AP_MOVE_FLAT+points;
          //walk towards target for diff units
          if (ap_needed > a.currentAP) {continue}
          var xtile = Math.round(a.currentX + ((row.actor.currentX - a.currentX) * (diff / dist)));
          var ytile = Math.round(a.currentY + ((row.actor.currentY - a.currentY) * (diff / dist)));
          console.log('xtile'+xtile+' ytile'+ytile);
          var chance = a.calcChanceHit(row.actor,in_hand) * Physics.calcGetThroughChance(xtile, ytile, row.actor.currentX,row.actor.currentY);
          var target = {"x":xtile, "y":ytile};
        } else {
          var hitchance = a.calcChanceHit(row.actor, in_hand);
          var reachable = Physics.calcGetThroughChance(a.currentX, a.currentY, row.actor.currentX, row.actor.currentY);
          var chance = hitchance*reachable;
          var target = {};
        }
        if (chance > bestshot) {
          bestshot = chance;
          selected_actor = row.actor;
          moveto = target;
        }
      }
      if (selected_actor) {
        if (Object.keys(target).length> 0) {
          return Action.create(bestshot,  "AI_MOVE_FIRE", target, row.actor, in_hand);
        } else {
          return Action.create(bestshot,  "AI_FIRE", {x:row.lastX,y:row.lastY}, row.actor, in_hand);
        }
      }
      return Action.create(0);
    },
    calcBestMeleeAttack: function(a) {
      var item = a.hasInventoryItem('melee');
      if (!item) {
        return Action.create(0);
      }
      var opponent =  this.GetClosestOpponent(a);
      //if we cant reach the target, return 0
      var is_reachable = Physics.findClosestLocation(a.currentX, a.currentY, opponent.lastX, opponent.lastY);
      if (Object.keys(is_reachable).length == 0) return 0;
      var aps = Points.getAPsToAttack(a, opponent.actor, item);
      console.log('will need aps: '+aps+' i have '+a.currentAP);
      if (aps > a.currentAP) return Action.create(0);
      if (opponent) {
        return Action.create(a.ChanceHandToHand(opponent.actor), "AI_MELEE", {x:is_reachable.x,y:is_reachable.y}, opponent.actor, item);
      } else {
        return Action.create(0);
      }
    },
    GetClosestOpponent: function(a) {
      var dist = Infinity;
      var target = "";
      for (row of a.getVisibleEnemies()) {
        var newdist = Physics.calculateDistance(a.currentX, a.currentY, row.actor.currentX, row.actor.currentY);
        if (newdist < dist) {
          dist = newdist;
          target = row;
        }
      }
      return target;
    },
    determineStrategy: function() {
      //if we see enemies: engage them
      //if we don't see enemies: search
      //move all to loc, search, follow orders
      if (this.visibleObjects.actors.length >0) {
        for (act of this.visibleObjects.actors) {
          if (!act.destroyed) {
            console.debug('change to attack strategy');
            this.strategy.type = "attack";
            this.strategy.location = {x: act.lastX, y: act.lastY};
            break;
          }
        }
      } else {
        this.strategy.type = "random";
      }
    }
  }
  return AI;
});
