/*
  ContentLoader.js

  read and initialize game ressources defined in json file
  handle sprites, init main ressources

  //room for improvement: do not return file/dictionary maps, but initialized objects
  //todo: optimize data structure of return value to be used with TileEngine.js
  //todo: make this flexible as the amount of game data increases
  */

define([
  '../res/Item'
],
       function(Item) {
         return function (filepath) {
           //for now, we take one json file as argument
           function initMap(filepath) {
             //this loads a map file ( will return simple json for now )
             //4 layers: tile, structure, item, actor
             return new Promise(function(resolve,reject) {
               var xmlhttp = new XMLHttpRequest();
               xmlhttp.open("GET", filepath, true);
               xmlhttp.send();
               xmlhttp.onload = function() {
                 if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                   var response = JSON.parse(xmlhttp.responseText);
                   resolve(response);
                 } else {
                   reject();
                 }
               }
             });
           }

           function loadSpriteSheet(image, width, height,dict) {
             return imageLoader([{
               spritesheet: 1,
               file: image,
               width: width,
               height: height,
               spritedict: dict
             }])
           }
           
           function initGameRes() {
             //this loads static game resources (sprites, items, tileproperties)
             return new Promise(function(resolve, reject) {
               var xmlhttp = new XMLHttpRequest();
               xmlhttp.open("GET", "json/gameresources.json", true);
               xmlhttp.send();
               xmlhttp.onload = function() {
                 if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                   var response = JSON.parse(xmlhttp.responseText);
                   //groundImages, items, objectImages, objects, spritesheets
                   //first init static game resources
                   var promises = [loadSpriteSheet(response.spritesheets.objectImages, 100,167),
                                   loadSpriteSheet(response.spritesheets.groundImages, 100,50),
                                   loadSpriteSheet(response.spritesheets.icons,46,46),
                                   loadSpriteSheet(response.spritesheets.actorImages,100,167, response.ActorFrameDefinition ),
                                   loadSpriteSheet(response.spritesheets.explosion,200,240)]
                   //when all our promises have resolved, continue
                   Promise.all(promises).then(function ([obj, tile, icon, actor, explo]) {
                     response.objectImagesLoaded = obj;
                     response.groundImagesLoaded = tile;
                     response.iconSpriteLoaded = icon;
                     response.actorSprites = actor;
                     response.explosionSprites = explo;
                     //todo: dictionary for actors: 4*front,4*back,4*left,4*right
                     //init items
                     response.itemlist = {};
                     for (i in response.itemDefinitions) {
                       response.itemlist[i] = new Item(response.itemDefinitions[i], 
                                                       response.iconSpriteLoaded.files);
                     }
                     //init object properties
                     response.objectImagesLoaded.objectProperties = [];
                     for (i of response.objectDefinitions) {
                       //use material and type definitions if they have been set
                       if (typeof i === "object") {
                         response.objectImagesLoaded.objectProperties.push(i);
                       } else {
                         response.objectImagesLoaded.objectProperties.push("");
                       }
                     }
                     //console.debug(response.objectImagesLoaded);
                     //init tile definitions
                     response.groundImagesLoaded.tileProperties = [];
                     for (i of response.tileDefinitions) {
                       response.groundImagesLoaded.tileProperties.push(i.material);
                     }
                     resolve (response);
                   });
                 } else {
                   reject();
                 }
               };
             });
           }

           function imageLoader(imageset) {
             return new Promise(function(resolve, reject) {
               var loaded = 0; // Images total the preloader has loaded
               var loading = 0; // Images total the preloader needs to load
               var images = [];
               var dict = [];
               var is_spritesheet = 0;
               loading += imageset.length;
               imageset.map(function(img) {
                 imgName = img;
                 if (typeof(img) === 'object') {
                   if (img.spritesheet) {
                     is_spritesheet = 1;
                   }
                   imgName = img.file.split("/").pop();
                   images[imgName] = new Image();
                   images[imgName].src = img.file;
                   dict.push(imgName);
                 } else {
                   imgName = img.split("/").pop();
                   images[imgName] = new Image();
                   images[imgName].src = img;
                   dict.push(imgName);
                 }
                 images[imgName].onload = function() {
                   loaded ++;
                   if (loaded === loading && !is_spritesheet) {
                     resolve({files: images, dictionary: dict});
                   }
                   else {
                     if (is_spritesheet) {
                       splitSpriteSheet({
                         files: images,
                         dictionary: dict,
                         width: img.width,
                         height: img.height,
                         offsetX: 0,
                         offsetY: 0,
                         spacing: 0,
                         firstgid: 0,
                         spritedict: img.spritedict                         
                       }).then(function(response) {
                         //console.debug(response);
                         resolve(response);
                       });
                     }
                   }
                 };
               });
             });
           }

           //todo: this needs some cleaning up and rewriting
           function splitSpriteSheet(spritesheet) {
             //console.debug(spritesheet);
             return new Promise(function(resolve, reject) {
               var loaded = 0; 
               var loading = 0;
               var images = [];
               var ctx = document.createElement('canvas');
               var tileManip;
               var imageFilePathArray = [];
               var spriteID = 0;
               var tileRow;
               var tileCol;
               //console.debug(spritesheet.files[spritesheet.dictionary[0]]);
               var spritesheetCols = Math.floor(spritesheet.files[spritesheet.dictionary[0]].width / (spritesheet.width));
               var spritesheetRows = Math.floor(spritesheet.files[spritesheet.dictionary[0]].height / (spritesheet.height));
               loading +=  spritesheetCols * spritesheetRows;
               ctx.width = spritesheet.width;
               ctx.height = spritesheet.height;
               tileManip = ctx.getContext('2d');
               for (var i = 0; i < spritesheetRows; i++) {
                 for (var j = 0; j < spritesheetCols; j++) {
                   tileManip.drawImage(spritesheet.files[spritesheet.dictionary[0]], 
                                       j * (spritesheet.width + spritesheet.offsetX  + spritesheet.spacing) + spritesheet.spacing, 
                                       i * (spritesheet.height + spritesheet.offsetY + spritesheet.spacing) + spritesheet.spacing, 
                                       spritesheet.width + spritesheet.offsetX - spritesheet.spacing, 
                                       spritesheet.height  + spritesheet.offsetY - spritesheet.spacing, 
                                       0, 
                                       0, 
                                       spritesheet.width, 
                                       spritesheet.height);
                     let spriteRef = spritesheet.spritedict !== undefined ?
                     spritesheet.spritedict[spriteID] :
                     spriteID;
                   imageFilePathArray[spriteRef] = spriteID;
                   images[spriteID] = new Image();
                   images[spriteID].src = ctx.toDataURL();
                   tileManip.clearRect (0, 0, spritesheet.width, spritesheet.height);
                   images[spriteID].onload = function () {
                     loaded ++;
                     if (loaded === loading) {
                       resolve({files: images, dictionary: imageFilePathArray});
                     }
                   };
                   spriteID ++;
                 }
               }
             });
           }
           //main call
           return {
             //load all game data
             initGameData: function(filepath) {
               return new Promise(function(resolve,reject) {
                 initGameRes().then(function(r) {
                   initMap(filepath).then(function(rmap) {
                     for (p in rmap) {
                       r[p] = rmap[p];
                     }
                     resolve(r);
                   });
                 });
               });
             },
             loadNewMap: function(filepath) {
               return initMap(filepath);
             }
           }
         }
       }
      );
