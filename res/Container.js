/*
  Container.js
  Clickable object with inventory and properties
*/

define([  '../res/GroundItemBatch'],
function(GroundItemBatch) {

  //constructor
  function Container(locked, model) {
    this.class = 'Container';
    this.id = 'chest';
    this.img = this.drawImage();
    this.isOpen = 0;
    this.locked = locked;
    this.lock_is_known = 0;
    this.inventory = [];
    this.model = model;
    var objectProperties = window.TileEngine.layers[1].objectProperties;
    for (var i in objectProperties) {
      if (objectProperties[i].id == this.model) {
        this.modelID = i;
        break;
      }
    }
    this.inventory.toJSON = ()=> {
      var out = [];
      for (var obj of this.inventory) {
        out.push({"obj":{
          "blueprint": {
            "index": obj.obj.blueprint.index,
            "itemClass":obj.obj.blueprint.itemClass,
          },
          "slot": obj.slot,
          "condition":obj.obj.condition
        }});
      }
      return out
    }
    this.destroyed = 0;
    this.discovered = 1;
  }

  Container.prototype = {
    constructor: Container,
    getRadialActions: function(actor) {
      var returnvalue = {
        'Open': 'open',
        'Bash': 'bash'
      };

      if ( this.locked && this.lock_is_known ) {
        if ( actor.hasInventoryItem('crowbar') ) {
          returnvalue["crowbar"] = 'crowbar';
        }

        if ( actor.hasInventoryItem('lockpick') ) {
          returnvalue["pick lock"] = 'picklock';
        }
      }
      return returnvalue;
    },
    open: function(a) {
        if (this.locked) {
          console.log('is locked');
        } else {
          this.isOpen = true;
          //don't overlap
          var pos = window.TileEngine.getObjLayout().getTilePos(a.actor.currentX+4,a.actor.currentY+4);
          window.TileEngine.drawInventoryPopover(this,pos.x,pos.y);
          a.actor.openCharacterInventory(a.actor.currentX,a.actor.currentY);
        }
    },
    bash: function() {
      console.log('bashing');
      //drop items to ground
      var container = new GroundItemBatch(this.inventory);
      window.TileEngine.spawn(container, this.currentX, this.currentY);
      this.destroyed = true;
    },
    drawImage: function(tileX,tileY) {
      //shadow
      if (tileX && tileY) {
        window.TileEngine.tileField[0].drawShadow(tileX,tileY,'rgba(255, 255, 0, 0.2)');
      }
      var sprites = window.TileEngine.gamedata.objectImagesLoaded;
      return sprites.files[this.modelID];
    },
    addItem: function(item) {
      this.inventory.push({obj: item});
    },
    EditorProperties: function() {
      return [
        {"name":"IsOpen","type":"integer","value":this.isOpen},
        {"name":"locked","type":"integer","value":this.locked}
      ]
    }
  }
  return Container;
});
