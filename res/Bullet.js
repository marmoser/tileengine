/*
  Bullet.js
  Bullet objects are currently active projectiles
*/

define(
function() {
  //constructor
  function Bullet(context, firer, target, start, dest, chance_to_hit) {
    /*
    @context: 2d canvas object for drawing
    @target: who or what are we shooting at
    @firer: who fired this bullet
    */
    this.context = context;
    this.target = target;
    this.firer = firer;
    this.start = start;
    this.speed = 15;
    //current should NOT be a pointer
    this.current = Object.create(start);
    this.lasthit = {};
    this.lasttile ={"x":"","y":""};
    this.chance_to_hit = chance_to_hit;
    //maybe we should calculate atan here?
    //and some hit/miss stuff
    var rndx = (Math.round(Math.random()) * 2 - 1)*Math.floor(Math.random() * Math.floor(30));
    var rndy = (Math.round(Math.random()) * 2 - 1)*Math.floor(Math.random() * Math.floor(30));
    this.dest = {x: dest.x+rndx/chance_to_hit, y: dest.y+rndy/chance_to_hit};
    //console.debug(dest);
  }

  Bullet.prototype = {
    move: function() {
      var angle = Math.atan2(this.dest.y - this.current.y, this.dest.x - this.current.x);
      var sin = Math.sin(angle)*this.speed;
      var cos = Math.cos(angle)*this.speed;
      
      this.current.x = Math.round(this.current.x+=cos);
      this.current.y = Math.round(this.current.y+=sin);
      
      //check if we hit something
      var currentTile = window.TileEngine.getXYCoords(this.current.x, this.current.y);
      var objects_here = window.TileEngine.objectMap[currentTile.x][currentTile.y];
      if (this.current.x > this.dest.x-5 && this.current.y > this.dest.y-5) {
        this.removeBullet();
      }
      if (this.lasttile.x == currentTile.x && this.lasttile.y == currentTile.y) {return}
      for (var i=0; i< objects_here.length; i++) {
        //we never hit ourselves and ignore dropped items
        if (objects_here[i].constructor.name == 'GroundItemBatch' ||
            objects_here[i] === this.firer) {
          continue
        }
        if (this.lasthit != objects_here[i]) {
          if (objects_here[i].destroyed) {continue;}
          //todo: some nice dice rolls here
          var dmg = window.random(60,10)*100;
          //agility saving throw
          if ((this.target.stance == 2 || this.target.stance == 3)&& 
            this.target.currentX != currentTile.x && this.target.currentY != currentTile.y) {
            //we are not aiming at this actor
              dmg-=20;
          }
          objects_here[i].applyDamage({health: dmg, iEP: dmg/4, iAP: 2});
          this.removeBullet();
        }
        this.lasthit = objects_here[i];
      }
      //check if we hit a static object
      var prop = window.TileEngine.gamedata.objectImagesLoaded.objectProperties;
      var objectmap = window.TileEngine.getObjLayout().mapLayout;
      var current_obj = objectmap[currentTile.x][currentTile.y]-1;
      if (current_obj > 0) {
        var obj_prop = prop[current_obj];
        if (obj_prop) {
          if (obj_prop.type === "wall") {
            console.debug('hit a wall');
            this.removeBullet();
          } else if (obj_prop.type === "tree") {
            console.debug('hit a tree');
            if (obj_prop.height >=2 && Math.random() < obj_prop.cover) {
              this.removeBullet();
            }
          }
        }
      }
     
      this.lasttile.x = currentTile.x;
      this.lasttile.y = currentTile.y;
    },
    draw: function() {
      var ctx = this.context;
      ctx.beginPath();
      ctx.fillStyle = "red";
      ctx.fillRect(this.current.x, this.current.y,3,3);
      ctx.stroke();
    },
    removeBullet: function() {
      var index = window.TileEngine.activeBullets.indexOf(this);
      if (index > -1) window.TileEngine.activeBullets.splice(index,1);
    }
  }
  return Bullet;
});
