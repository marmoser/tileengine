/*
  tactical engine
  draw tiles, setup everything
*/
define([
  '../res/TileEngine',
  '../res/ContentLoader',
  '../res/InputHandler',
  '../res/Actor',
  '../res/Physics'
    ],
       function(TileEngine, ContentLoader, InputHandler, Actor, Physics) {
    return function() {

      //helper for window redraw
      window.requestAnimFrame = (function() {
        return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame  ||
        window.mozRequestAnimationFrame     ||
        window.oRequestAnimationFrame       ||
        window.msRequestAnimationFrame      ||
        function(callback, element) {
          window.setTimeout(callback, 62);
        };
      })();
        
      window.random = function(max, min)  {
        return (Math.floor(Math.random() * (max - min + 1)) + min)/100;
      };
      
      //init main stuff
      function _bootstrap(map) {
        //hacky: disable contextmenu
        //document.oncontextmenu = function() {return false;};
        //load content files
        this.contentLoader = new ContentLoader();
        this.contentLoader.initGameData(map).then(function(r) {
          redrawui = 0;
          //init TileEngine
          window.TileEngine = new TileEngine(window.innerWidth, window.innerHeight*0.8, r);
          //load first map
          window.TileEngine.initLayers(r);
          //create input handler
          this.input = new InputHandler(document, window.TileEngine.context);
          var xrange = parseInt(window.innerWidth/50);
          var yrange = parseInt(window.innerWidth/50);
          window.TileEngine.range = {x:xrange,y:yrange};
          window.TileEngine.render();
          window.TileEngine.drawUI();
          for (a of window.TileEngine.actors) {
            Physics.getVisibleObjects(a,0,1);
          }
        });
      }
      
      //load a new map file
      function _loadNewMap(map) {
        this.contentLoader.loadNewMap(map).then(function(r) {
          console.debug(r);
          window.TileEngine.initLayers(r);
        });
      }

      function _updateDimensions(w, h) {
      var canvas = window.TileEngine.context.canvas;
      window.TileEngine.range = {x:parseInt(w/50),y:parseInt(h/50)};
      canvas.width = w;
      canvas.height = h*0.8;
    }

    return {
      //wrappers for internal functions
      bootstrap: _bootstrap,
      updateDimensions: _updateDimensions,
      loadNewMap: _loadNewMap
    };

}}
);
