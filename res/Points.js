/*
  Points.js
  methods for AP calculation
*/

define(['../res/Physics'],
       function(Physics) {
  return {
	  AP_TALK: 6,
	  AP_ITEM_CATCH: 5,
	  AP_ITEM_TOSS: 8,
	  AP_CROUCH: 2,
	  AP_STAND: 2,
	  AP_PRONE: 2,
	  AP_RELOAD: 5,
	  AP_START_AID: 5,
	  AP_STOP_AID: 3,
	  AP_GET_HIT: 2,
	  AP_OPEN: 5,
    AP_CLOSE: 5,
	  AP_PICKLOCK: 10,
	  AP_EXAMINE_DOOR: 5,
	  AP_FORCE_DOOR: 8,
	  AP_CROWBAR: 10,
	  AP_PICKUP_ITEM: 3,
    AP_DROP_ITEM: 3,
	  AP_GIVE_ITEM: 5,
    AP_THROW_ITEM: 10,
	  AP_UNTRAP: 10,
	  AP_MIN: 10,
	  AP_MAX: 26,
	  AP_MOVE_FLAT: 3,
	  AP_MOVE_GRASS: 4,
	  AP_MOVE_BUSH: 5,
	  AP_MOVE_RUBBLE: 6,
	  AP_MOVE_SWIM: 8,
	  AP_MOVE_WADE: 9,
	  AP_PUNCH: 10,
	  AP_USE_KNIFE: 10,
    AP_FIRE_GUN: 15,
	  OKBREATH: 10,
	  OKLIFE: 15,
	  OKCONSCIOUS: 10,
	  DEFAULT_AIMSKILL: 80,
    AP_TURN: 2,
	  //energy
	  EP_MOVE_FLAT: 5,
	  EP_MOVE_GRASS: 10,
	  EP_MOVE_BUSH: 20,
	  EP_MOVE_RUBBLE: 35,
	  EP_MOVE_SWIM: 100,
	  EP_MOVE_WADE: 75,
	  EP_STANCE: 10,
	  EP_AIM: 5,
	  EP_RELOAD: 20,
	  EP_FIRST_AID_START: 0,
	  EP_GET_HIT: 200,
	  EP_GET_WOUNDED: 50,
	  //door
	  EP_OPEN_DOOR: 30,
	  EP_PICKLOCK: -250,
	  EP_EXAMINE_DOOR: -250,
	  EP_FORCE_DOOR: 200,
	  EP_CROWBAR: 350,
	  EP_UNLOCK: 50,
	  EP_EXPLODE_DOOR: -250,
	  EP_UNTRAP: 150,
	  EP_LOCK_DOOR: 50,
	  //rest
	  EP_SETUP_MINE: 250,
	  EP_DISARM_MINE: 50,
	  EP_FIRE_GUN: 25,
	  EP_PUNCH: 100,
	  EP_FIRE_BIGGUN: 50,
	  EP_USE_KNIFE: 200,
	  EP_THROW_ITEM: 50,
	  getAPsForAction: function (action, actor, target, coords) {
	    var slotitem = '';
	    if (actor.constructor.name == 'Actor') {
        slotitem = window.TileEngine.findForInventorySlot(actor.inventory, 'slot_lhand');
      }
      var pos = window.TileEngine.getObjLayout().getXYCoords(coords.x,coords.y);
	    var path = Physics.findPath([actor.currentX,actor.currentY],[pos.x,pos.y]);
	    var cost = Physics.movementCosts(path);
	    switch (action) {
      case 'attack':
		    return this.getAPsToAttack(actor, target, slotitem);
		    break;
      case 'dressWound':
		    return this.getAPsToChangeStance(actor, 2) + this.AP_START_AID;
		    break;
      case 'toggleCrouch':
		    return this.getAPsToChangeStance(actor,2);
		    break;
      case 'toggleCrawl':
		    return this.getAPsToChangeStance(actor,3);
		    break;
      case 'toggleStand':
		    return this.getAPsToChangeStance(actor,1);
		    break;
      case 'give':
		    return this.AP_GIVE_ITEM+cost;
		    break;
      case 'open':        
        return this.AP_OPEN+cost;
		    break;
      case 'close':
        return this.AP_CLOSE+cost;
        break;
      case 'force':
		    return this.AP_FORCE_DOOR+cost;
		    break;
      case 'picklock':
		    return this.AP_LOCKPICK+cost;
		    break;
      case 'openinv':
		    return 0;
		    break;
      case 'moveto':
        if (actor.stance === 3) return 0;
        return cost;
        break;
      case 'bash':
        return this.AP_FORCE_DOOR;
        break;
      case 'look':
        return this.AP_TURN;
        break;
	    }
      console.debug("points not found (!) for "+action);
	    return 10;
	  },
	  getAPsToChangeStance: function (actor, target) {
	    //1 = stand, 2 = crouch, 3 = lie
	    if (target == actor.stance) return 0;
	    if (actor.stance == 3 && target == 1) return this.AP_CROUCH+this.AP_STAND;
	    if (actor.stance == 3 && target == 2) return this.AP_CROUCH;
	    if (actor.stance == 1 && target == 3) return this.AP_CROUCH+this.AP_STAND;
	    if (actor.stance == 1 && target == 2) return this.AP_CROUCH;
	    if (actor.stance == 2 && target == 3) return this.AP_PRONE;
	    if (actor.stance == 2 && target == 1) return this.AP_STAND;
	  },
	  getAPsToAttack: function (actor, target, slotitem) {
      if (typeof slotitem != 'undefined') {
        switch (slotitem.blueprint.itemClass) {
        case 'weapon':
          return this.AP_FIRE_GUN;
          break;
        case 'melee':
          var path = Physics.findPath([actor.currentX,actor.currentY],[target.currentX,target.currentY]);
          var cost = Physics.movementCosts(path);
          return this.AP_USE_KNIFE + cost;
          break;
        case 'throwing':
          return this.AP_THROW_ITEM;
          break;
        }
      } else {
        return this.AP_PUNCH;
      }
      // //todo: slotitem can be null
	    // var itemtype = slotitem.blueprint.itemClass;
	    // var cost = ;
	    // switch (itemtype) {
	    // case 'weapon':
		  //   if (!slotitem.isReady) {
		  //     cost += 5;
		  //   }
		  //   break;
	    // }
	    // if (typeof slotitem == 'undefined') {
		  //   return cost;
	    // } else {
		  //   cost += 10; //aim
	    // }
	    // return cost;
	  },
    Closest: function(num, arr) {
      //helper function to determine which number is closest
      var curr = arr[0];
      var diff = Math.abs (num - curr);
      for (var val = 0; val < arr.length; val++) {
        var newdiff = Math.abs (num - arr[val]);
        if (newdiff < diff) {
          diff = newdiff;
          curr = arr[val];
        }
      }
      return curr;
    }
  }
});
