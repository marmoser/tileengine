//Worker for pathfinding, draft

self.addEventListener('message', function(evt) {
  /*
  map: object map,
  tiles: tile map
  */
  var start = evt.data.start,
  dest = evt.data.end,
  map = evt.data.map,
  tiles = evt.data.tileinfo;
    /*
    Nodes -
    x: x position
    y: y position
    p: Index of the parent node (Stored in closed array)
    g: Cost from start to current node
    h: Heuristic cost from current node to destination
    f: Cost from start to desination going through current node
  */
  var Tilenode = function (x, y, p, g, h, f) {
    this.x = x;
    this.y = y;
    this.p = p;
    this.g = g;
    this.h = h;
    this.f = f;
  };
  
  var distance = function (current, end) {
    var x = current.x - end.x,
    y = current.y - end.y;
    var thistile = tiles[current.x][current.y];
    return (Math.pow(x, 2) + Math.pow(y, 2))*thistile.iAP;
  };
  
  var startnode = new Tilenode(start[0], start[1], -1, -1, -1, -1); // Start Node
  var endnode = new Tilenode(dest[0], dest[1], -1, -1, -1, -1); // End Node
  
  var cols = map.length-1, // Get number of rows from map
    rows = map[0].length-1, // Number of columns from map
    o = [], // Open Nodes
    c = [], // Closed Nodes
    mn = new Array(rows*cols), // Store open/closed nodes
    g = 0,
    h = distance(startnode, endnode),
    f = g + h;
  
  if (map[endnode.x][endnode.y] !== 0) {
    //find closest reachable tile
    var found = 0;
    for (let pos of [{x: endnode.x+1, y:endnode.y},{x: endnode.x-1, y:endnode.y}, {x: endnode.x, y:endnode.y+1}, {x: endnode.x, y:endnode.y-1}]) {
      if (pos.x < 0 || pos.y < 0 || map[pos.x][pos.y] !== 0) continue
      var tmpnode = new Tilenode(pos.x, pos.y,-1,-1,-1,-1);
      if ( distance(startnode, tmpnode) < h) {
        h = distance(startnode,tmpnode);
        endnode = tmpnode;
        found = 1;
      }
    }
    if (!found) {
      console.log('unreachable, stop');
      self.postMessage([]); // No Path Found!
      self.close();
      return true;
    }
  }
  //MODIFICATIONS END
  // Place start node onto list of open nodes
  o.push(startnode);
  
  while (o.length > 0) {

    // Locate Best Node
    var best = {
        c: o[0].f,
        n: 0
      };

    for (var i = 1, len = o.length; i < len; i++) {
      if (o[i].f < best.c) {
        best.c = o[i].f;
        best.n = i;
      }
    }

    // Set current to best
    var current  = o[best.n];

    // we've reached the end
    if (current.x === endnode.x && current.y === endnode.y) {
      var path = [{x: endnode.x, y: endnode.y}]; // Create Path 
      // Loop back through parents to complete the path
      while (current.p !== -1) {
        current = c[current.p];
        path.unshift({x: current.x, y: current.y});
      }
      self.postMessage(path); // Return the path
      self.close();
      return true;
    }

    // Remove current node from open list
    o.splice(best.n, 1);
    mn[current.x + current.y * rows * cols] = false; // Set bit to closed

    c.push(current);
    // Search new nodes in all directions
    for (var x = Math.max(0, current.x-1), lenx = Math.min(cols, current.x+1); x <= lenx; x++) {
      for (var y = Math.max(0, current.y-1), leny = Math.min(rows, current.y+1); y <= leny; y++) {
          // Check if location square is open
          if (Number(map[x][y]) === 0) {

            // Check if square is in closed list
            if (mn[x + y * rows * cols] === false) {
              continue;
            }

            // If square not in open list use it
            if (mn[x + y * rows * cols] !== true) {
              var n = new Tilenode(x, y, c.length-1, -1, -1, -1); // Create new node
              n.g = current.g + Math.floor(Math.sqrt(Math.pow(n.x - current.x, 2) + Math.pow(n.y-current.y, 2)));
              n.h = distance(n, endnode);
              n.f = n.g + n.h;

              o.push(n); // Push node onto open list

              mn[x + y * rows * cols] = true; // Set bit into open list
            }

          }
      }
    }
  }
  
  self.postMessage([]); // nothing found
  self.close();
  return true;
});