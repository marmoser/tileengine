/*
  wrapper around Field
  adds interactive objects
*/
define([
  '../res/Layer',
  '../res/Actor',
  '../res/Container',
  '../res/GroundItemBatch',
  '../res/Door',
  '../res/Bullet',
  '../res/PopupText',
  '../res/Points',
  '../res/TacticalAI'
  ],
  function (TileField, Actor, Container, GroundItemBatch, Door, Bullet, PopupText, Points, AI) {

    //factions have members, relationships between hostile and friendly and an AI
    function Faction (name, AI) {
      this.name = name;
      this.members = [];
      this.AI = AI;
      //helper for iterating faction members
      this.iter = 0;
    }
    Faction.prototype = {
      constructor: Faction,
      addMember: function(a) {
        if (!this.members.includes(a) && a.constructor.name === 'Actor') {
          this.members.push(a);
        } else {
          console.log('warning: actor could not be added to faction '+this.name);
	      }
      },
      hostilityTowards: function(f){
        //returns a value between 0 and 1
        //is always mutual
        if (f.constructor.name === 'Faction') {
          if ( (this.name == 'player' && f.name == 'enemy') || (f.name == 'player' && this.name == 'enemy') ) {
            return 0;
          }
        }
      },
      removeMember: function(a) {
        var pos = this.members.indexOf(a);
        if (pos >= 0) {
          this.members.splice(pos,1);
        }
      }
    }
    //helper object for spawnRadialMenu when clicking on a tile
    function TileObject() {
      return {
        getRadialActions: function(actor, coords) {
          return {"move":"moveto",
                  "look":"look"};
        },
        id: "Tile" 
      }
    }

    //constructor
    function TileEngine (w,h,gamedata) {
      this.layers = {};
      this.layersToJSON = ()=> {
        //transform layer information for json map export
        var output = {};
        output.groundLayerDefinition = this.getTileLayout().mapLayout;
        output.structureLayerDefinition = this.getObjectComposite();
        //add interactive objects
        for (var i=0; i<this.objectMap.length; i++) {
          for (var j=0; j<this.objectMap[i].length; j++) {
            var elem = this.objectMap[i][j];
            if (elem.length > 0 && elem[0].constructor.name != 'Actor') {
              output.structureLayerDefinition[i][j] = elem[0];
            }
          }
        }
        output.actorDefinitions = [];
        for (a of this.actors) {
          output.actorDefinitions.push({"class": a.constructor.name, "id": a.id, "x":a.currentX, "y":a.currentY,
                                        "dir":a.orientation, "faction":a.faction.name, "inventory":a.inventory});
        }
        return output;
      }
      this.tileField = new Array();
      this.enemyAI = new AI('enemy');
      this.context = this.createCanvas("canvaselement", w, h);
      this.gamedata = gamedata;
      this.itemlist = gamedata.itemlist;
      this.tileinfo = gamedata.tileDefinitions;
      //stores interactive objects and actors in 2d array form, multiple can be in one position
      this.objectMap = [];
      //stores actors
      this.actors = [];
      //stores all objects
      this.objects = [];
      //stores explosions, each object contains x,y and animation sprite counter
      this.explosions = [];
      this.selected = [];
      this.hideObjects = false;
      this.activeBullets = [];
      this.activePopupTexts = [];
      
      this.time = ({
        offset: Date.now(),
        elapsed: function() {
          return Math.round((Date.now() - this.offset)/1000);
        }
      });
      this.mode = 'turnbased'; //turnbased, realtime
      //which part of the map should be rendered?
      this.starttile = {x:0, y:0};
      this.shadow_rgba = function() {
        return this.time.elapsed() % 600 / 600;
      };
    }

    TileEngine.prototype = {
      constructor: TileEngine,
      initActors: function(actorDefs) {
        //read actors from map json and item them
        var actors = [];
        this.playerfaction = new Faction('player');
        this.enemyfaction = new Faction('enemy', this.enemyAI);
        this.TileObject = TileObject();
        //now init actors
        for (a of actorDefs) {
          if (a.class === 'Actor') {
            if (a.faction === 'player') {
              var faction = this.playerfaction;
            } else {
              var faction = this.enemyfaction;
            }
            var actor = new Actor(a.id, a.x,a.y,a.dir,faction,faction.AI);
            faction.addMember(actor);
            if (typeof a.inventory != 'undefined') {
              for (i of a.inventory) {
                var index =  i.obj.blueprint.index;
                var slot = i.obj.slot;
                if (typeof slot != 'undefined') {
                  actor.addItem(this.itemlist[index].spawnNew(), slot);
                } else {
                  actor.addItem(this.itemlist[index].spawnNew());
                }
              }
            }

            actor.healthpopup = this.addPopupText(actor.health, {x:0, y:0}, 'white' , '' ,0);
            this.objectMap[a.x][a.y].push(actor);
            actors.push(actor);
          }
        }
        return actors;
      },
      initObjects: function(objectDefs) {
        //init objects for objectDefs
        //static objects: walls, trees...
        //interactive objects: doors, containers
        //blocking vs nonblocking (corpses, debris) objects
        //only one object per tile
        //objectMap -> 2dim array with objects in game as array
        //mapLayout -> 2dim array with objects from map json
        var mapLayout = new Array(objectDefs);
        this.objectMap = new Array(objectDefs);
        for (var i = 0; i < objectDefs.length; i++) {
          mapLayout[i] = new Array(objectDefs[i].length);
          this.objectMap[i] = new Array(objectDefs[i].length);
          for (var j = 0; j < objectDefs[i].length; j++) {
            this.objectMap[i][j] = new Array();
            if ( typeof objectDefs[i][j] == 'object' ) {
              if (objectDefs[i][j].class === 'Container') {
                var container = new Container(objectDefs[i][j].locked);
                for (var item of objectDefs[i][j].inventory) {
                  //todo: this is suboptimal
                  container.inventory.push({obj: this.itemlist[item.obj.blueprint.index].spawnNew()});
                }
                this.objectMap[i][j].push(container);
                this.objects.push(container);
              } else if (objectDefs[i][j].class === 'GroundItemBatch') {
                  var items = new Array();
                  var container = new GroundItemBatch(objectDefs[i][j].items);
                  this.objectMap[i][j].push(container);
                  this.objects.push(container);
              } else if (objectDefs[i][j].class === 'Door') {
                  var door = new Door(objectDefs[i][j]);
                  this.objectMap[i][j].push(door);
                  this.objects.push(door);
              } else {
                mapLayout[i][j] = objectDefs[i][j].id;
              }
            } else {
              mapLayout[i][j] = objectDefs[i][j];
            }
          }
        }
        //console.debug(this.objects);
      },
      createCanvas: function(element,w,h) {
        //console.debug('creating canvas');
        var canvasElement = document.createElement('canvas');
        if (!(canvasElement.getContext && canvasElement.getContext('2d'))) {
            let errmsg = "Browser is not supporting canvas";
            document.getElementById(element).innerHTML = errmsg;
            throw(errmsg);
        }
        canvasElement.style.border = "1px solid #000000";
        canvasElement.width = w;
        canvasElement.height = h;
        return document.getElementById(element).appendChild(canvasElement).getContext('2d');
      },
      initLayers: function(r) {
        //this allows dynamic loading of different maps
        this.activePopupTexts = [];
        this.layers = [{
          title: "tiles",
          layout: r.groundLayerDefinition,
          graphics: this.gamedata.groundImagesLoaded.files,
          graphicsDictionary: this.gamedata.groundImagesLoaded.dictionary
        },
        {
          title: "objects",
          layout: r.structureLayerDefinition,
          graphics: this.gamedata.objectImagesLoaded.files,
          graphicsDictionary: this.gamedata.objectImagesLoaded.dictionary,
          objectProperties: this.gamedata.objectImagesLoaded.objectProperties,
          zeroIsBlank: true
        }];
        
        this.initObjects(r.structureLayerDefinition);
        this.actors = this.initActors(r.actorDefinitions);
        this.explosionSprites = r.explosionSprites;
  
        for (var i = 0; i < this.layers.length; i++) {
          this.tileField[i] = new TileField( this.context, this.context.canvas.width, this.context.canvas.height, this.layers[i]);
        }
        this.centerView();

      },
      spawn: function(obj,i,j) {
        console.debug('will spawn '+i +' ' +j);
        console.debug(obj.constructor.name);
        this.objectMap[i][j].push(obj);
      },
      draw: function(tileX, tileY) {
        //draw tile layer  
        this.tileField[0].draw(tileX, tileY);
        this.tileField[0].drawShadow(tileX,tileY,'rgba(0, 0, 0, '+this.shadow_rgba()+')');
       
        //draw objects
        if (this.hideObjects) return;
          var currentObject = this.objectMap[tileX][tileY];
          if (currentObject.length > 0) {
            for (var i=0;i<currentObject.length;i++) {
              //do not render destroyed objects
              if (currentObject[i].destroyed) {
                console.debug('destroyed');
                var indexof = this.objectMap[tileX][tileY].indexOf(currentObject[i]);
                //console.debug(indexof);
                this.objectMap[tileX][tileY].splice(this.objectMap[tileX][tileY].indexOf(currentObject[i]), 1);
                continue
              }
              //hide undiscovered items
              //if (currentObject[i].discovered === false) continue
              var image = currentObject[i].drawImage(tileX,tileY);
              //image can come from the object ( complex rendering ) or a simple tile 
              if (image instanceof Image) {
                this.tileField[1].draw(tileX, tileY, image);
              } else {
                this.tileField[1].draw(tileX, tileY, this.layers[1].graphics[image]);
              }
            }
          } else {
             this.tileField[1].draw(tileX, tileY);
          }
         
      },
      render: function() {
        //renders everything (layers), for editor windows we render less
        var ctx = this.context;
        //if (typeof ctx == 'undefined') return;
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        this.moveStep();
        var starttilex = this.starttile.x-10 <0 ? 0 : this.starttile.x-10;
        var starttiley = this.starttile.y-10 <0 ? 0 : this.starttile.y-10;
        //todo: only redraw what has changed
        for (var i = starttilex; i < this.range.x+this.starttile.x; i++) {
          if (i < 0 || i >= this.getMapSize()) continue;
          for (var j = starttiley; j < 0 + this.range.y+this.starttile.y; j++) {
            if (j < 0 || j >= this.getMapSize()) continue;
            this.draw(i,j);
            }
        }
        //draw actors by iterating and calling the drawImage method for each
        for (a of this.actors) {
          if (a.currentX >= this.starttile.x && a.currentX <=  this.range.x+this.starttile.x &&
              a.currentY >= this.starttile.y && a.currentY <=  this.range.y+this.starttile.y) {
            let d = a.drawImage();
            this.tileField[1].draw(d.x,d.y, d.img);
          }
        }
        //draw explosions
        for (e of this.explosions) {
          this.tileField[0].draw(e.x,e.y,this.explosionSprites.files[e.state]);
          e.state = e.state+1;
          if (e.state == 15) {
            //remove explosion object
            var index = this.explosions.indexOf(e);
            this.explosions.splice(index, 1);
          }

        }
        this.drawBullets();
        this.drawPopupText();
        if (typeof redrawui !== 'undefined' && redrawui == '1') this.drawUI();
        //without binding the context of render to TileEngine, ctx will be undefined
        var bound_render = this.render.bind(this);
        //how often should the canvas be redrawn?
        setTimeout(function() {
          window.requestAnimFrame(bound_render);
        }, 1000/20);
      },
      drawUI: function() {
        if (window.MainClass == 'EditorMain'){return}
        redrawui = 0;
        //draw ui for each actor
        var navbar = $("#navbar");
        navbar.empty();
        var width = navbar.width();
        var maxslots = 8;
        var pcactors = this.getPCActors();
        for ( var i=0; i<pcactors.length; i++ ) {
          var slot = $("<div class='slot'></div>");
          slot.css('width',width/maxslots+'px');
          slot.append( pcactors[i].generateCharacterStatusbar() );
          navbar.append(slot);
        }

        //fill remaining space with slots
        for (var x = pcactors.length; x<maxslots; x++) {
          var p = width/maxslots;
          //get data from actor an display
          var slot = $("<div class='slot'></div>");
          slot.css('width',width/maxslots+'px');
          navbar.append(slot);
        }

        //header
        var headerbar = $("#navbar_header");
        headerbar.empty();
        headerbar.append(
          $("<div>", {class: "buttons"}).css("float","right").add(
            $("<a>", {class: "button", id: "end_turn", html: "end turn"}).css("float","right")
            .click( function (e) {
              window.TileEngine.endTurn();
            })
          )
        ).append(
          $("<div>", {class: "clock", html: "00:00"}).css("float","left")
        );
      },
      centerView: function() {
        for (var i in this.tileField) {
          //console.debug(this.tileField[i]);
          this.tileField[i].align(this.starttile.x,this.starttile.y);
        }
        return;
      },
      getXYCoords: function(XPosition, YPosition) {
        //get coords of tile layer
        return this.tileField[0].getXYCoords(XPosition, YPosition);
      },
      move: function(direction, distance) {
        for (var i=0; i < this.tileField.length; i++) {
           this.tileField[i].move(direction, distance);
        }
      },
      scroll: function(dir,dist) {
          //scroll in one direction by adjusting this.starttile
	      switch (dir) {
          case "up":
              if (this.starttile.y + 5 > 0) this.starttile.y--;
              break;
          case "down":
              //10 for statusbar
              if (this.starttile.y - 10 < this.getMapSize() - this.range.y/2) this.starttile.y++;
              break;
          case "left":
              if (this.starttile.x + 5 > 0) this.starttile.x--;
              break;
          case "right":
              if (this.starttile.x < this.getMapSize() - this.range.x/2) this.starttile.x++;
              break;
        }
        this.centerView();
      },
      isInteractiveObjects: function(x,y) {
        //returns interactive objects for coords
        //more than one object per square is possible (eg actor in door)
        var out = [];
        for (var a of this.actors) {
          if (a.currentX == x && a.currentY == y) out.push(a);
        }
        var obj = this.objectMap[x][y];
        if (Array.isArray(obj)) {
          for (var i=0;i<obj.length;i++) {
            if (!out.includes(obj[i])) out.push(obj[i]);
          }
        }
        return out;
      },
      getObjLayout: function() {
        return this.tileField[1];
      },
      getTileLayout: function() {
        return this.tileField[0];
      },
      moveStep: function() {
        for (var i= 0; i < this.actors.length; i++) {
          var currentX = this.actors[i].currentX;
          var currentY = this.actors[i].currentY;
          //if (this.actors[i].faction === 'player') 
          //this.actors[i].RayOfVisibility();
          this.actors[i].moveStep();
          if (!this.actors[i]) continue;
          if (currentX != this.actors[i].currentX|| currentY != this.actors[i].currentY) {
            redrawui = 1;
            //update position of actor in internal arrays
            var index = this.objectMap[currentX][currentY].indexOf(this.actors[i]);
            this.objectMap[currentX][currentY].splice(index, 1);
            if (this.objectMap[this.actors[i].currentX][this.actors[i].currentY] === undefined) {
              this.objectMap[this.actors[i].currentX][this.actors[i].currentY] = new Array();
            }
            this.objectMap[this.actors[i].currentX][this.actors[i].currentY].push(this.actors[i]);
          }
          //now update objectMap
        }
      },
      //return an array of player controlled characters
      getPCActors: function() {
        return this.playerfaction.members;
        /*var pcactors = [];
        for (var i= 0; i < this.actors.length; i++) {
          if ( this.actors[i].faction.name == 'player' ) {
            pcactors.push(this.actors[i])
          }
        }
        return pcactors;*/
      },
      findForInventorySlot: function (inv, slot) {
        //returns item currently in slot @slot
        for (var i=0; i< inv.length; i++) {
          if (inv[i].slot == slot) {
            return inv[i].obj;
          }
        }
      },
      drawInventoryPopover: function (obj,coordX,coordY) {
      //draws inventory popover for obj
        var row =   $("<div>", {class: "inventory-row"});

        //callback for receiving a dnd item
        var callback_receive = function( event, ui ) {
          var existing_item = ui.item.next(".inventory-item").length > 0 ? ui.item.next(".inventory-item") : ui.item.prev(".inventory-item")  ;
          ui.sender.append(existing_item);
        };
        //global, is used by other dnd fields too
        sortable = {
          connectWith: '.inventory-cell',
          receive: callback_receive,
          out: function (e,u) {
            outside = true;
            $('.ui-sortable-helper').css("cursor","crosshair");
          },
          over: function (e,u) {
            outside = false;
             $('.ui-sortable-helper').css("cursor","pointer");
          },
          beforeStop: function (e,u) {
            if (outside) {
              //throw / drop
              if (obj.currentAP < Points.AP_DROP_ITEM) {
                window.TileEngine.addPopupText('not enough points, need'+Points.AP_DROP_ITEM, obj.getPositionInPx());
              } else {
                var c =  window.TileEngine.getXYCoords(e.pageX,e.pageY);
                var closest = obj.findClosestLocation(c.x,c.y);
                //create new GroundItemBatch
                var container = new GroundItemBatch(u.item.data('content'));
                //add container to object map
                window.TileEngine.spawn(container, closest.x,closest.y);
                //remove this item from sortable
                u.item.remove();
                obj.SubtractPoints(Points.AP_DROP_ITEM);
                redrawui = 1;
              }
            } else {
              var itemslot = u.item.data('content').blueprint.slot;
              /*if (typeof itemslot == 'undefined') {
                //for attachments
                itemslot = u.item.data('content').blueprint.itemClass;
              }*/
              console.debug(itemslot);
              //items with a slot only fit into the right inventory slot or the generic slots
              if (itemslot) {
                console.debug ( u.item.parent().data('slot') );
                //item requires a slot that is not allowed here!
                if (u.item.parent().data('slot') &&
                  u.item.parent().data('slot') != 'slot_'+itemslot) {
                    console.debug('does not fit here');
                    return false;
                  }
              } else {
                //item requires no slot but might be not allowed in target
                var thisslot =  u.item.parent().data('slot');
                console.debug(thisslot);
                if (thisslot && thisslot !== 'slot_lhand') {
                  console.debug('not allowed');
                  return false;
                }
              }
            }
          },
          start: function () {
            $(".inv-popover").hide();
          }
        };
        //callback for closing inventory popover
        var close_callback = function (e) {
          var spanelement = $(this).closest("span");
          spanelement.removeClass("open");
          //rebuild internal inventory
          obj.inventoryOpen = 0;
          obj.inventory = [];
          //get slots for this popover
          spanelement.find(".inventory-item").each(function(e,i) {
            var object = $(this).data('content');
            var slot = $(this).parent().data('slot');
            if (typeof(object) != 'undefined' && object.constructor.name == 'ItemInstance' && typeof obj.addItem === 'function') {
              slot ? obj.addItem(object,slot) :
                obj.addItem(object);
            }
          });
          if (typeof obj.closeFunction ==='function') {
            console.debug('will call');
            obj.closeFunction();
          }
        };
        //callback for closing item details popover
        var close_callback_item = function (e) {
          var spanelement = $(this).closest("span");
          var item = spanelement.data('item');
          console.debug(item);
          spanelement.removeClass("open");
          //rebuild internal inventory
          item.attachments = [];
          //get slots for this popover
          spanelement.find(".inventory-item").each(function(e,i) {
            var object = $(this).data('content');
            var slot = $(this).parent().data('slot');
            var slotchunks = slot.split("_");
            console.debug(slotchunks);
            if (typeof(object) != 'undefined' && object.constructor.name == 'ItemInstance') {
               item.attachments.push( {'type':slotchunks[1], 'item': object} )
               if (slot == 'ammo') {
                 console.debug('will reload');
                  item.reload(object);
               }
            } else {
               item.attachments.push( {'type':slotchunks[1], 'item': 'undefined'} )
            }
          });
        };
        var details_callback = function(e) {
          if (e.button === 2) {
            console.debug('right click');
            var item = $(this).data('content');
            //new popup with details, attachment system
            var popover = $("<span>", {class: "popover-wrapper open"}).css({top: coordY-100, left: coordX, position:'absolute', clear: 'both'})
        .data('item', item)
        .append(
          $("<div>", {class: "popover-modal popover-target"}).append(
            $("<div>", {class: "popover-header", html: "Details for "+item.blueprint.name}).append(
              $("<a>", {href: "#", "data-toggle-role": "close", style: "float: right", html: "x"}).click(close_callback_item)
            )
          ).append(
            $("<div>", {class: "popover-body", id: "popovercontent"}).append(
              $("<div>", {class: "inventory-table"})
              .append(item.generateDetails())
            )
          )
        );
        popover.insertBefore($("#navbar"));
            return false;
          }
        }
        if (obj.constructor.name == 'Actor') {
          row.append(
            $("<div>", {class: "inventory_column"}).append(
              $("<div>", {class: "inventory-cell inventory-slot_helmet", title: "Head Protection"}).append(
                  $("<div>", {class: "inventory-item"})
              ).data('slot', 'slot_helmet')
              .add(
                $("<div>", {class: "inventory-cell inventory-slot_body", title: "Body Protection"}).append(
                  $("<div>", {class: "inventory-item"})
                ).data('slot', 'slot_body'))
              .add(
                $("<div>", {class: "inventory-cell inventory-slot_legs", title: "Legs Protection"}).append(
                  $("<div>", {class: "inventory-item"})
                ).data('slot', 'slot_legs')
              )
            )
          ).append(
            $("<div>", {class: "inventory_column"}).append(
              $("<div>", {class: "inventory-cell inventory-slot_lhand", title: "Left Hand"}).append(
                  $("<div>", {class: "inventory-item"})
              ).data('slot', 'slot_lhand')
              .add(
                $("<div>", {class: "inventory-cell inventory-slot_rhand", title: "Right Hand"}).append(
                  $("<div>", {class: "inventory-item"})
                ).data('slot', 'slot_rhand')
              )
            )
          );
          //console.debug(row.find(".inventory-slot_helmet"));
          //now fill the slots with items
          for (let slot of obj.slots) {
            var slotobj = this.findForInventorySlot(obj.inventory, slot);
            var slotrow = row.find(".inventory-"+slot);
            if (slotobj !== undefined) {
              slotrow.children().first()
              .css({
                   "background-image": "url('" + slotobj.blueprint.image.src + "')",
                   "background-size" : "contain",
                   "background-repeat": "no-repeat",
                   "background-position": "center"
              })
              .addClass(slotobj.blueprint.name)
              .data('content', slotobj)
              .append(
                //quickinfo popovernew Ac
                $("<div>", {class: "inv-popover"}).append(
                  $("<p>", {html: slotobj.generateQuickinfo()})
                )
              )
              .hover(function(i) {
                //show/hide description popover
                $(".inv-popover").hide();
                $(this).children(":first").toggle();
              }, function(i) {
                $(this).children(":first").hide();
              })
              .mousedown(details_callback)
            }
            slotrow.sortable(sortable);
          }
        }
        var invlength = obj.constructor.name == 'Actor' || obj.constructor.name == 'Container' ? 6 : obj.inventory.length;
        for (var x = 0; x < invlength; x++) {
          if (obj.inventory[x]) {
            if (obj.inventory[x].slot) continue;
            //this slot is not empty
            row.append(
              $("<div>", {class: "inventory-cell inventory-slot_generic"}).append(
                $("<div>", {class: "inventory-item"}).css({
                 "background-image": "url('" + obj.inventory[x].obj.blueprint.image.src + "')",
                   "background-size" : "contain",
                   "background-repeat": "no-repeat",
                   "background-position": "center"
                })
                .addClass(obj.inventory[x].obj.blueprint.name)
                .data('content', obj.inventory[x].obj)
                .append(
                //quickinfo popover
                $("<div>", {class: "inv-popover"}).append(
                  $("<p>", {html: obj.inventory[x].obj.generateQuickinfo()})
                  )
                )
                .hover(function(i) {
                  //show/hide description popover
                  $(".inv-popover").hide();
                  $(this).children(":first").toggle();
                }, function(i) {
                  $(this).children(":first").hide();
                })
                .mousedown(details_callback)
              ).sortable(sortable)
            );
          } else {
            row.append(
              $("<div>", {class: "inventory-cell inventory-slot_generic"}).append(
                $("<div>", {class: "inventory-item"}).css(
                "background-image", "none"
                )
              ).sortable(sortable)
            )
          }
        }
        var popover = $("<span>", {class: "popover-wrapper open"}).css({top: coordY, left: coordX, position:'absolute', clear: 'both'})
        .append(
          $("<div>", {class: "popover-modal popover-target"}).append(
            $("<div>", {class: "popover-header", html: "Inventory of "+obj.id}).append(
              $("<a>", {href: "#", "data-toggle-role": "close", style: "float: right", html: "x"}).click(close_callback)
            )
          ).append(
            $("<div>", {class: "popover-body", id: "popovercontent"}).append(
              $("<div>", {class: "inventory-table"})
              .append(row)
            )
          )
        );
        popover.insertBefore($("#navbar"));
      },
      spawnRadialMenu: function(interactiveObjects, coords, selected) {
        //interactiveObject: the object
        //selected: the subject
        //rework of old loadRadialMenu
        //draw links for all actions and attach listeners
        var l = 0;
        for (interactiveObject of interactiveObjects) {
          //determine total number of actions
          var actions = interactiveObject.getRadialActions(selected,coords);
          var l = l+Object.keys(actions).length;
        }
        var i = 0;
        var circle = $("<div>", {class: "circle open"});
        var menu = $("#circular");
        menu.empty();
        //fill this empty menu with actions from interactiveObjects
        for (interactiveObject of interactiveObjects) {
          var actions = interactiveObject.getRadialActions(selected,coords);
          for (index in actions) {
            i++;
            var aps = Points.getAPsForAction(actions[index],selected, interactiveObject, coords);
            if (aps == 0 && actions[index] != 'openinv') {continue}
            circle.append(
              $('<a data-action="'+actions[index]+'">'+index+'('+aps+'P)'+'</a>')
                .click(function(e,i) {
                  //handle click
                  var action = e.target.getAttribute('data-action');
                  var iobj = $(this).data('iobj')
                  //before we call the action, determine if we have enough APs
                  if (window.TileEngine.mode == 'turnbased') {
                    var aps_needed = Points.getAPsForAction(action, selected, iobj, coords);
                    console.debug('need ap'+aps_needed+' current'+selected.currentAP);
                    if (selected.currentAP >= aps_needed) {
                      selected.callAction(action, iobj, coords);
                    } else {
                      window.TileEngine.addPopupText('not enough points, need '+aps_needed, selected.getPositionInPx());
                    }
                  } else {
                    selected.callAction(action, iobj, coords);
                  }
                  var menu = $(this).closest("nav");
                  $(menu).hide();
                  e.preventDefault();
                })
                .data('iobj', interactiveObject)
                .css(
                  {
                    "left": (50 - 25*Math.cos(-0.5 * Math.PI - 2*(1/l)*i*Math.PI)).toFixed(4) + "%",
                    "top" : (50 + 25*Math.sin(-0.5 * Math.PI - 2*(1/l)*i*Math.PI)).toFixed(4) + "%"
                  }
                )
            )
          }
        }
        circle.append(
          $("<a>X</a>")
            .click(function(e,i) {
              var menu = $(this).closest("nav");
              $(menu).hide();
              e.preventDefault();
            })
            .css({
              "top": '55%',
              "left" : '50%'
            })
        );

        menu.append(
          $("<nav>", {id: "radialmenu", class: "circular-menu"})
            .append(circle)
            .css({left:(coords.x-120) + "px", top:(coords.y-120) + "px"})
            .show()
        )
      },
      exportCurrentMap: function() {
        //export currently loaded map to json
        var jsondata = JSON.stringify(this.layersToJSON());
        var data = new Blob([jsondata], {type: 'application/json'});
        var textFile = window.URL.createObjectURL(data);
        var link = document.createElement('a');
        document.body.appendChild(link);
        link.style = 'display:none';
        link.href = textFile;
        link.download = 'map.json';
        link.click();
        window.URL.revokeObjectURL(textFile);
       },
      getObjectComposite: function() {
        //blend static and dynamic objects into array showing which tiles are available
        //clone the results of the getLayout array pointer first
        var layout = this.getObjLayout().mapLayout.map( function (arr) {
          return arr.slice();
        });

        for (var i=0; i< layout.length; i++) {
          for (var j=0; j<layout[i].length; j++) {
            var c = this.objectMap[i][j];
            if (Array.isArray(c) && typeof c[0] == 'object') {
              if ((c[0].class == 'Door' && c[0].state == 'open') ||
                 c[0].class == 'GroundItems') {
                layout[i][j] = 0;
              } else {
                layout[i][j] = 1;
              }
            } else if (isNaN(c)) {
              layout[i][j] = 1;
            }
          }
        }
        for (var a of this.actors) {
          layout[a.currentX][a.currentY] = 1;
        }
        return layout;
      },
      getTileMovementCosts: function() {
        //return movement costs for tile layer in 2d array
        var weightedtiles = new Array();
        var tiles = Object.create(this.getTileLayout().mapLayout);
        for (var i=0; i< tiles.length; i++) {
          weightedtiles[i] = new Array();
          for (var j=0; j<tiles[i].length; j++) {
            var tiledetails = this.tileinfo[ tiles[i][j] ] ;
            if (typeof tiledetails == 'undefined') {
              weightedtiles[i][j] = {iAP: Points.AP_MOVE_FLAT, iEP: Points.EP_MOVE_FLAT};
            } else {
              switch (tiledetails.material) {
                case 'floor':
                  weightedtiles[i][j] = {iAP: Points.AP_MOVE_FLAT, iEP: Points.EP_MOVE_FLAT};
                break;
                case 'street':
                  weightedtiles[i][j] = {iAP: Points.AP_MOVE_FLAT, iEP: Points.EP_MOVE_FLAT};
                break;
                case 'water':
                  weightedtiles[i][j] = {iAP: Points.AP_MOVE_SWIM, iEP: Points.EP_MOVE_SWIM};
                break;
                case 'soil':
                  weightedtiles[i][j] = {iAP: Points.AP_MOVE_BUSH, iEP: Points.EP_MOVE_BUSH};
                break;
                case 'stone':
                  weightedtiles[i][j] = {iAP: Points.AP_MOVE_FLAT, iEP: Points.EP_MOVE_FLAT};
                break;
                case 'sand':
                  weightedtiles[i][j] = {iAP: Points.AP_MOVE_RUBBLE, iEP: Points.EP_MOVE_RUBBLE};
                break;
                case 'grass':
                  weightedtiles[i][j] = {iAP: Points.AP_MOVE_GRASS, iEP: Points.EP_MOVE_GRASS};
                break;
                case 'flowers':
                  weightedtiles[i][j] = {iAP: Points.AP_MOVE_BUSH, iEP: Points.EP_MOVE_BUSH};
                break;
              }
            }
          }
        }
        return weightedtiles;
      },
      drawBullets: function() {
        for (var bullet of this.activeBullets) {
          bullet.move();
          bullet.draw();
        }
       
      },
      spawnBullet: function(action) {
        //calculate chance to hit
        var chance = action.actor.calcChanceHit(action.target,action.object);
        var start_pos = this.getObjLayout().getTilePos(action.actor.currentX,action.actor.currentY);
        var dest_pos = this.getObjLayout().getTilePos(action.target.currentX,action.target.currentY);
        var bullet = new Bullet(this.context, action.actor, action.target, start_pos, dest_pos, chance);
        this.activeBullets.push(bullet);
        action.object.decreaseAmmo(1);
      },
      addPopupText: function (text, position, color, size, duration) {
        var popup = new PopupText(this.context, text, position, color, size, duration);
        this.activePopupTexts.push(popup);
        return popup;
      },
      drawPopupText: function () {
        for (var text of this.activePopupTexts) {
          text.draw();
        }
      },
      endTurn: function () {
        if (window.MainClass == 'EditorMain'){return}
        var elapsed = window.TileEngine.time.elapsed();
        //console.debug(elapsed);
        //remove visible objects for AI
        this.enemyAI.determineStrategy();
        this.enemyAI.visibleObjects = {"actors": [], "objects": []};
        for ( act in this.actors ) {
          this.actors[act].prepareTurn();
        }
        if (this.enemyfaction.members.length > 0) {
          var act = this.enemyfaction.members[0];
          //console.debug(act);
          act.AI.decideAction(act);
          act.AI.executeAction(act);
          //if we have more faction members, prepare iteration
          if (this.enemyfaction.members.length >1) {
            this.enemyfaction.iter++;;
          }
        }
        redrawui = 1;
      },
	    getMapSize: function() {
	      return this.getTileLayout().mapLayout.length;
	    },
      spawnActor: function(faction) {
        console.log('spawn '+faction);
        //creates enemy at random location between 1 and MapSize()-1
        var x = Math.floor(Math.random()*this.getMapSize()-1)+1;
        var y = Math.floor(Math.random()*this.getMapSize()-1)+1;
        if (faction == 'enemy') {
          var a  = new Actor('enemy', x,y,0,this.enemyfaction, this.enemyAI);
          this.enemyfaction.addMember(a);
        }
        if (faction == 'player') {
          var a = new Actor('player', x,y,0,this.playerfaction, null);
          this.playerfaction.addMember(a);
        }
        a.createInventory();
        a.healthpopup = window.TileEngine.addPopupText(a.health, {x:0, y:0}, 'white' , '' ,0);
        this.actors.push(a);
      },
      removeActor: function(a) {
        //remove actor from actorlist, update AI visibility
        var index = this.actors.indexOf(a);
        //if (index > -1) this.actors.splice(index,1);
        var visindex = this.enemyAI.visibleObjects.actors.indexOf(a);
        if (visindex > -1) this.enemyAI.visibleObjects.actors.splice(visindex,1);
        this.enemyAI.determineStrategy();
        if (typeof a.faction != 'undefined') {
          a.faction.removeMember(a);
        }
      },
      addExplosion: function(x,y) {
        this.explosions.push({'x':x,'y':y,'state':0});
      }
    }
    return TileEngine;
  }
);
