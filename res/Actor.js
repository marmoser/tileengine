/*
  Actor Class
  actors can have:
  -spritesheets
  -stats
  -inventory
  -ai control vs user control
  -action points
  -action queue
  -Faction
  -draw at position
  -move to position
  -die


  //require deps: inventory, actions
  */
//todo: load inventory, actions here
//define([...,...],function
define([
  '../res/GroundItemBatch',
  '../res/Points',
  '../res/Physics'
], function(GroundItemBatch, Points, Physics) {
  /*
    constructor
  */
  function Actor(id, x, y, dir, faction, ai) {
    //faction should be an object linking an AI instance
    //faction {name: 'suckers', AI: obj}
    
    //safety checks: Actor constructor must be called with new keyword
    if (!(this instanceof Actor)) {
      throw new TypeError("constructor cannot be called as a function.");
    }
    this.id = id;
    
    //attach ai, setup stats
    this.AI = ai;
    this.AIStack = {
      action: null,
      lastAction: null,
      actionTarget: null,
      actionLocation: null
    };
    
    this.currentX = x;
    this.currentY = y;
    //direction from 0 to 359
    this.orientation = dir;
    this.visibleObjects = {actors: [], objects: []};
    this.bleeding = 0;
    this.OKLIFE = 10;
    this.path = [];

    this.targetX = x;
    this.targetY = y;

    this.is_selected = 0; /*currently controlled by player*/

    this.stance = 1; /*1 stand, 2 crouch, 3 lie*/

    this.faction = faction;

    this.inventory = [];
    //limit attributes to be exported
    this.inventory.toJSON = ()=> {
      var out = [];
      for (var obj of this.inventory) {
        out.push({"obj":{
          "blueprint": {
            "index": obj.obj.blueprint.index,
            "itemClass":obj.obj.blueprint.itemClass,
          },
          "slot": obj.slot,
          "condition":obj.obj.condition
        }});
      }
      return out
    }
    this.inventoryOpen = 0;
    //slots for inventory
    this.slots = ['slot_helmet','slot_body','slot_legs','slot_lhand','slot_rhand'];
    //keep track of this actor's exploits, currently not used
    this.records = {
      iKillsHostiles: 0,
      iShotsFired: 0,
      iShotsHit: 0,
      iKnifeAttacks: 0,
      iNrBattles: 0,
      iTimesWounded: 0,
      iLocksPicked: 0,
      iTrapsRemoved: 0,
      iNrDetonations: 0
    };

    //actor's stats
    this.stats = {
      iStrength: 60+Math.round(Math.random()*40),
      iHealth : 100,
      iEnergy : 100,
      iExplosives : Math.round(Math.random()*100),
      iWisdom : Math.round(Math.random()*100),
      iMarksmenship : 70+Math.round(Math.random()*30),
      iMedical : Math.round(Math.random()*100),
    };

    //current attribute values for
    //.stats -> max values
    this.health = 70;
    this.energy = 100;
    this.collapsed = 0;
    this.iExpr = 1;
    //experience
    
    this.currentAP = this.calculateAPs();
    this.actionqueue = [];
    this.currentAction = '';
    
    this.healthpopup = '';
    this.giveaid = ''; //we are servicing target actor
    this.receiveaid = ''; //we are being serviced by a doctor
    
    this.distanceMoved = 0;
    this.nextbleed = '';
  }


  Actor.prototype = {
    //repoint the constructor
    constructor: Actor,

    setPosition: function(x, y) {
      this.currentX = x;
      this.currentY = y;
    },

    setFaction: function(myfaction) {
      this.faction = myfaction;
    },

    calculateAPs: function() {
      if (this.health < 10) return 0;
      var ubPoints = 20 + (((10 * this.iExpr + 3 * this.health + 2) + 5) / 10);
      //todo:equipment bonuses
      //weight: if we carry more than 100%, we get fewer AP
      if (this.currentWeight > 100) {
        ubPoints = (ubPoints * 100) / this.currentWeight ;
      }
      //injury
      if (this.health < this.stats.iHealth) {
        var bBandage = this.stats.iHealth - this.health - this.bleeding;
        ubPoints -= (2 * ubPoints * (this.stats.iHealth - this.health + (bBandage / 2))) /
          (3 * this.stats.iHealth);
      }
      //fatigue
      ubPoints -= (ubPoints * (100 - this.energy)) / 200;
      //enforce min/max values
      if (ubPoints > Points.AP_MAX) ubPoints = Points.AP_MAX;
      if (ubPoints < Points.AP_MIN) ubPoints = Points.AP_MIN;
      return Math.ceil(ubPoints);
    },
    /*
      calculate movement agility for this Actor
      @returns: integer
    */
    calcAgility: function() {
      var weight = this.currentWeight();
      return weight;
    },
    /*
      weight of current equipment
      percent of actor's total carrying capacity
    */
    currentWeight: function() {
      var weight = 0;
      for (var i=0; i< this.inventory.length; i++) {
        weight += this.inventory[i].obj.blueprint.weight;
      }
      var percent = (weight / this.stats.iStrength)*100;
      return percent;
    },
    calcNextBleed: function() {
      var bandaged = this.stats.iHealth - this.health - this.bleeding;
      var nextbleed = (this.health + bandaged /2) / (10 + this.distanceMoved);
      console.log('nextbleed '+nextbleed);
      return nextbleed;
    },
    prepareTurn: function() {
      //console.log(this.id);
      //this.getVisibleObjects();
      var blood = 0;
      if (this.bleeding > 15 || this.health < 10) {
        if (this.receiveaid == '') {
          blood = this.bleeding - 30;
          this.nextbleed--;
          if (this.nextbleed <= 0) {
            var bandaged = this.stats.iHealth - this.bleeding - this.health;
            if (bandaged > 0 && this.health >= 10) {
              this.bleeding++;
              console.log(this.id+' bleeds');
            } else {
              //not bandaged, or dying
              if (this.health < 10) {
                this.bleeding++;
                this.stats.iHealth--;
              }
            }
            this.nextbleed = this.calcNextBleed();
          }
        }
      }
      this.currentAP = this.calculateAPs();
      this.lastAction = [];
      if (this.health > 10 && this.energy > 10) this.collapsed = 0;
      if (this.health < 10 || this.energy < 10) this.collapsed = 1;
      
      //continue first aid
      if (this.giveaid != '') {
        if (this.health < 10) return;
        if (Physics.calculateDistance(this.currentX, this.currentY, this.giveaid.target.currentX, this.giveaid.target.currentY) > 2) return;
        console.log('dress wound continue');
        this.dressWound(this.giveaid);
      }
    },
    performNextAction: function() {
      if (this.actionqueue.length === 0) {
        console.log('no action queued');
        return;
      }
      //      console.log('performnext');
      var n = this.actionqueue[0];
      //console.debug(n);
      this.currentAction = n;
      if (n.action == 'moveto') {
        this.move(n.x,n.y);
        //we only terminate move after we've reached the target
      } else {
        //call the action on this object
        if (n.target) {
          n.target[n.action](n);
        } else {
          n.actor[n.action](n);
        }
        //clean up the action queue
        n.actor.terminateAction();
      }
    },
    terminateAction: function() {
      this.actionqueue.shift();
      this.lastAction = this.currentAction;
      this.currentAction = [];
      //if we have enough AP for another turn, decide again
      if (this.faction.name == 'enemy' && this.currentAP >= 15) {
        this.AI.decideAction(this);
        if (this.AIStack.action == 'AI_MOVE' && this.lastAction.action == 'moveto') {
          this.currentAP = 0;
        } else {
          this.AI.executeAction(this);
        }
      }
      if (this.actionqueue.length > 0) {
        this.performNextAction();
      } else {
        //for ai factions continue with next actor
        if (this.faction.name == 'enemy' && this.faction.iter > 0) {
          var nextactor = this.faction.members[this.faction.iter];
          if (nextactor) {
            nextactor.AI.decideAction(nextactor);
            if (nextactor.AIStack.action == 'AI_MOVE' && nextactor.lastAction.action == 'moveto') {
              nextactor.currentAP = 0;
            } else {
              nextactor.AI.executeAction(nextactor);
            }
          }
          if (this.faction.members.length < this.faction.iter) {
            this.faction.iter++;
          } else {
            this.faction.iter = 0;
          }
        }
      }
    },
    callAction: function(action, target,e) {
      //action will be added to action queue of this
      /*
        1st action: walk to target, 2nd action: perform action
        actions can be paused and later resumed
      */
      var tileLayer = window.TileEngine;
      console.debug(this.id + ' performs ' + action + ' on ' + target.id);
      var targetpos = window.TileEngine.getXYCoords(e.x,e.y);
      //simple actions: attack_shoot, attack_punch, attack_throw...
      //results depend on the currently equipped item
      var slotitem = tileLayer.findForInventorySlot(this.inventory, 'slot_lhand');
      if (action == 'attack' && typeof slotitem != 'undefined' 
          && ((slotitem.blueprint.itemClass == 'weapon') 
              || (slotitem.blueprint.itemClass == 'throwing'))) {
        //ranged actions
        console.log('weapon');
        this.actionqueue.push({'actor': this, 'action': action, 'target':target, 'x': e.x, 'y': e.y, 'object':slotitem});
      } else if (target.id == "Tile" ||
                 (target.class == "GroundItems" && action == 'moveto' ) ||
                 (target.class == "Door" && action == 'moveto' )) {
        this.actionqueue.push({'actor': this, 'action': action, 'x': targetpos.x, 'y': targetpos.y});
      } else {
        //close-up actions
        var distance = Physics.calculateDistance(this.currentX, this.currentY, targetpos.x, targetpos.y);
        //walk to target
        if (distance > 2) {
          var cpos = Physics.findClosestLocation(
            this.currentX, this.currentY, targetpos.x, targetpos.y);
          if (cpos == "") {return};
          this.actionqueue.push({'actor': this, 'action': 'moveto', 'x': cpos.x, 'y': cpos.y});
        }
        if (slotitem) 
          this.actionqueue.push({'actor': this, 'action': action, 'target':target, 'x': e.x, 'y': e.y, 'object':slotitem});
        else
          this.actionqueue.push({'actor': this, 'action': action, 'target':target, 'x': e.x, 'y': e.y});
      }
      this.performNextAction();
    },
    /*
      get Actions for radial menu
      returns: attr:value pairs
    */
    getRadialActions: function(caller) {
      //todo: put this into faction class, faction alignment can change dynamically
      //console.debug(caller.constructor.name);
      if (caller === this) {
        var rval = {
          'open inventory': 'openinv'
        }
        switch (this.stance) {
        case 1:
        case 3:
          rval["toggle crouch"] = "toggleCrouch";
          break;
        case 2:
          rval["toggle stand"] = "toggleStand";
          rval["lie down"] = "toggleCrawl";
          break;
        }
        if (caller.hasInventoryItem('ammo') && caller.hasInventoryItem('weapon')) {
          rval["reload"] = "doreload";
        }
        return rval;
      }

      if (caller.constructor.name === 'Actor') {
        switch (this.faction.name) {
        case 'player':
          var rval = {
            'give item': 'give'
          }
          if ( caller.hasInventoryItem('medkit') && this.bleeding > 0) {
            rval["bandage"] = "dressWound";
          }
          return rval;
          break;
        case 'enemy':
          return {
            attack: 'attack'
          }
          break;
        }
      }
      //movement option when clicking on tiles
      if (caller.constructor.name === 'Object') {
        return caller;
      }
    },
    look: function(a) {
      //change orientation to look at a.x,a.y
      var angle = Math.atan2(a.y - this.currentY, a.x - this.currentX);
      angle = angle/(Math.PI/180);
      angle = angle < 0 ? angle-=90 : angle+=90;
      var closest =  Points.Closest(Math.ceil(angle), [0,90,180,270,360]);
      this.orientation = closest;
      this.SubtractPoints(Points.AP_TURN, 0);
    },
    move: function(toX, toY) {
      //console.log('moveto'+toX+' '+toY +'from '+this.currentX +' '+this.currentY);
      if (isNaN(toX) || isNaN(toY)) return;
      if (this.stance === 3) {
        this.terminateAction();
        return;
      }
      this.path = [];
      this.targetX = toX;
      this.targetY = toY;
      //pathfind returns an array with all nodes to traverse
      var result = Physics.findPath([this.currentX, this.currentY], [toX, toY]);
      if (result.length > 0) {
        this.path = result;
      } else {
        console.trace();
        console.log('unreachable, terminate'+toX+' '+toY+' '+this.currentX+' '+this.currentY);
        this.terminateAction();
      }
    },
    /*
      move one square
    */
    moveStep: function() {
      if (this.path.length) {
        for (var i = 0; i < this.path.length; i++) {
          //stop
          //todo: this should pause the action, not terminate it
          if (window.TileEngine.mode == 'turnbased' && this.currentAP<=Points.AP_MOVE_FLAT) {
            //console.debug('stop');
            this.path = [];
            this.terminateAction();
            break;
          }
          //we set current positions to the next node we have to traverse
          if (this.path[i].x == this.currentX && this.path[i].y == this.currentY) {
            //before we move, adjust the orientation
            if ( this.path[i + 1].x > this.path[i].x ) { //move one step forward on x
              this.orientation = 90;
            } else if ( this.path[i + 1].x < this.path[i].x ) {
              this.orientation = 270;
            } else if ( this.path[i + 1].y < this.path[i].y ) {
              this.orientation = 0;
            } else if ( this.path[i + 1].y > this.path[i].y ) {
              this.orientation = 180;
            }
            this.currentX = this.path[i + 1].x;
            this.currentY = this.path[i + 1].y;
            var movementcosts = window.TileEngine.getTileMovementCosts()[this.currentX][this.currentY];
            //console.debug(movementcosts);
            this.SubtractPoints(movementcosts.iAP,movementcosts.iEP/100);
            //this.currentAP -= 10;
            this.distanceMoved++;
            var visibleEnemies = this.getVisibleEnemies().length.valueOf();
            Physics.getVisibleObjects(this,0,1);
            //console.debug('i see '+this.getVisibleEnemies().length +' ' +visibleEnemies);
            if (this.getVisibleEnemies().length > visibleEnemies) {
              //have we sighted a new enemy? interrupt.
              window.TileEngine.addPopupText('enemy visible!',this.getPositionInPx());
              this.path = [];
              this.terminateAction();
              if (this.AI) {
                this.actionqueue = [];
                this.AI.determineStrategy();
                this.AI.decideAction(this);
                this.AI.executeAction(this);
              }
              break;
            }
            if ((i + 2) == this.path.length) {
              this.path = [];
              console.log('reached target, will terminate');
              this.terminateAction();
            }
            break;
          }
        }
      }
    },
    drawImage: function() {
      //draw a sprite for this actor with the correct orientation
      var pos = window.TileEngine.getObjLayout().getTilePos(this.currentX,this.currentY);
      this.healthpopup.position = {x: pos.x+40, y: pos.y-60};
      if (this.faction.name == 'player') {
        //display the name for pcs
        this.healthpopup.text = this.id;
      }
      var sprites = window.TileEngine.gamedata.actorSprites;
      var dirsprite = "";
      switch (this.orientation) {
      case 0:
        dirsprite = "back"
        break;
      case 90:
        dirsprite = "right";
        break;
      case 180:
        dirsprite = "front";
        break;
      case 270:
        dirsprite = "left";
        break;
      }

      if (this.destroyed) {
        dirsprite += "4";
      } else {
        var slotitem = window.TileEngine.findForInventorySlot(this.inventory, 'slot_lhand');
        if (typeof slotitem == "undefined") {
          dirsprite += "1";
        } else {
          if (slotitem.blueprint.itemClass == 'weapon') {
            dirsprite += "2";
          } else if (slotitem.blueprint.itemClass == 'melee') {
            dirsprite += "3";
          } else {
            dirsprite += "1";
          }
        }
        if (this.stance == 2) {
          dirsprite = "c"+dirsprite;
        } else if (this.stance == 3) {
          if (this.orientation >= 180)
            dirsprite = "lfront1";
          else
            dirsprite = "lfront2";

        }
      }
      if (this.is_selected) {
        //highlight selected actor
        window.TileEngine.tileField[0].drawShadow(this.currentX, this.currentY,'rgba(255, 0, 0, 0.1)');
      }
      var ref = sprites.dictionary[dirsprite];
      return {x:Number(this.currentX), y:Number(this.currentY),img:sprites.files[ref]};
    },
    //add item to inventory
    addItem: function(item, slot) {
      slot ? this.inventory.push({obj: item, slot: slot}) : this.inventory.push({obj: item});
    },
    //remove this item from the inventory
    removeItem: function(item) {
      for (var i=0; i< this.inventory.length; i++) {
        if ( this.inventory[i].obj == item) this.inventory.splice(i,1);
      }
    },
    //equip item in hand
    equipItem: function(item) {
      for (var i=0; i< this.inventory.length; i++) {
        if ( this.inventory[i].obj == item) this.inventory[i].slot = 'slot_lhand';
      }
    },
    clearInventory: function() {
      //define slots! helmet, west, legs, hands, pockets
      this.inventory = [];
    },
    openCharacterInventory: function(x,y) {
      if (this.inventoryOpen == 0) {
        this.inventoryOpen = 1;
        var pos = window.TileEngine.getObjLayout().getTilePos(x,y);
        //build this dynamically 1)popover 2)slots 3)content
        window.TileEngine.drawInventoryPopover(this, pos.x,pos.y);
      }
    },
    //generate statusbar bars for this actor
    generateCharacterStatusbar: function() {
      return $("<div>", {class: "port"}).append(
        $("<div>", {class: "portimage", html: this.id + (this.is_selected ? " (selected)" : " (unselected)") })
          .hover(function (i) {
            //show the stats
            var top = $(this).offset().top-110;
            var left = $(this).offset().left;
            $(this).children(":first").css({top: top, left: left, position:'absolute', clear: 'both'});
            $(this).children(":first").toggleClass("open");
          }).append(
            $("<span>", {class: "popover-wrapper"}).append(
              $("<div>", {class: "popover-modal popover-target"}).append(
                $("<div>", {class: "popover-body", id: "popovercontent"}
                 ).append( $("<p>", {class: "statinfo"}).text('Health: '+this.health))
                  .append( $("<p>", {class: "statinfo"}).text('Energy: '+this.energy))
                  .append( $("<p>", {class: "statinfo"}).text('Strength: '+this.stats.iStrength))
                  .append( $("<p>", {class: "statinfo"}).text('Wisdom: '+this.stats.iWisdom))
                  .append( $("<p>", {class: "statinfo"}).text('Marksmenship: '+this.stats.iMarksmenship))
                  .append( $("<p>", {class: "statinfo"}).text('Explosives: '+this.stats.iExplosives))
                  .append( $("<p>", {class: "statinfo"}).text('Medical: '+this.stats.iMedical))
              ).css("width", "150px")
            )
          )
      ).append(
        (window.TileEngine.mode == 'turnbased' ? $("<div>", {class: "aps", html: this.currentAP})
         .attr("title", this.currentAP+" remaining action points")
         .css("text-align", "center") : ''
        )
      ).add(
        $("<div>", {class: "stats"}).append(
          $("<div>", {class: "healthbar statbar"}).append(
            $("<div>", {class: "cval"})
              .css("height", (this.stats.iHealth-this.health)/this.stats.iHealth*100+"%")
          ).attr("title", "Health:"+this.health+" of "+this.stats.iHealth)
        ).append(
          $("<div>", {class: "energybar statbar"}).append(
            $("<div>", {class: "cval"}).css("height", (this.stats.iEnergy-this.energy)/this.stats.iEnergy*100+"%")
          ).attr("title", "Energy:"+this.energy+" of "+this.stats.iEnergy)
        ).append(
          $("<div>", {class: "pointbar statbar"}).append(
            $("<div>", {class: "cval"}).css("height", (Points.AP_MAX-this.currentAP)/Points.AP_MAX*100+"%")
          ).attr("title", "Points:"+this.currentAP+" of "+Points.AP_MAX)
        )
      );
    },
    getPositionInPx: function() {
      var pos = window.TileEngine.getObjLayout().getTilePos(this.currentX,this.currentY); 
      return {x: pos.x, y:pos.y};
    },
    closeFunction: function() {
      //inventory is closed -> terminate current action
      this.terminateAction();
    },
    applyDamage: function(dmg) {
      //redraw popup
      v = Math.floor(dmg.health);
      //this.healthpopup.text = v;
      //damages can affect health, energy and stats
      var pos = window.TileEngine.getObjLayout().getTilePos(this.currentX,this.currentY);
      window.TileEngine.addPopupText('-'+v, this.getPositionInPx());
      this.health-=v;
      if (this.health < 0) this.health=0;
      this.healthpopup.text = this.health;
      if (this.energy < 10 || this.health < 10) this.collapsed = 1;
      this.bleeding = v;
      this.nextbleed = this.calcNextBleed();
      redrawui = 1;
      //subtract points for getting hit
      this.SubtractPoints(dmg.iAP, dmg.iEP);
      if (this.health <= 0) {
        this.die();
      }
    },
    die: function() {
      /*end of actor's lifecycle
        we spawn a grounditem container and despawn the actor
      */
      if (this.destroyed) return;
      var container = new GroundItemBatch(this.inventory);
      window.TileEngine.spawn(container, this.currentX, this.currentY);
      this.destroyed = 1;
      this.is_selected = 0;
      //if we are dead, we don't need a health popup anymore
      this.healthpopup.removeText();
      window.TileEngine.removeActor(this);
      //console.debug(window.TileEngine.actors);
      window.TileEngine.spawnActor(this.faction.name);
    },
    hasInventoryItem: function(itemtype) {
      //does this actor have a item of this type
      //@returns item or undefined
      for (var i=0; i< this.inventory.length; i++) {
        if ( this.inventory[i].obj.blueprint.itemClass == itemtype ) return this.inventory[i].obj;
      }
    },
    getVisibleEnemies: function() {
      return this.visibleObjects.actors;
    },
    addVisibleEnemy: function(enemy) {
      if (this.visibleObjects.actors.indexOf(enemy) === -1 &&
          window.TileEngine.actors.indexOf(enemy) === -1) {
        this.visibleObjects.actors.push(enemy);
        if (this.AI) {
          if (this.AI.visibleObjects.actors.indexOf(enemy) === -1) {
            this.AI.visibleObjects.actors.push(enemy);
          }
        }
      }
    },
    removeVisibleEnemy: function(enemy) {
      if (enemy.destroyed) {
        this.visibleObjects.actors.splice(this.visibleObjects.actors.indexOf(enemy),1);
        if (this.AI) {
          this.AI.visibleObjects.actors.splice(this.AI.visibleObjects.actors.indexOf(enemy),1);
        }
      }
    },
    findClosestLocation: function(x,y) {
      //wrapper for physics function
      return Physics.findClosestLocation(x,y,this.currentX,this.currentY);
    },
    //actions start here
    give: function(a) {
      var distance = Physics.calculateDistance(a.actor.currentX, a.actor.currentY, a.target.currentX, a.target.currentY);
      if (distance > 3) {
        window.TileEngine.addPopupText('out of range', this.getPositionInPx());
      } else {
        a.actor.openCharacterInventory(a.actor.currentX,a.actor.currentY);
        a.target.openCharacterInventory(a.actor.currentX+4,a.actor.currentY+4);
        this.SubtractPoints(Points.AP_GIVE_ITEM, 0);
      }
    },
    openinv: function(a) {
      this.openCharacterInventory(this.currentX, this.currentY);
    },
    toggleCrouch: function(a) {
      this.SubtractPoints(Points.getAPsToChangeStance(a.actor, 2), Points.EP_STANCE);
      this.stance = 2;
    },
    toggleCrawl: function(a) {
      this.SubtractPoints(Points.getAPsToChangeStance(a.actor, 3), Points.EP_STANCE);
      this.stance = 3;
    },
    toggleStand: function(a) {
      this.SubtractPoints(Points.getAPsToChangeStance(a.actor, 1), Points.EP_STANCE);
      this.stance = 1;
    },
    attack: function(a) {
      if (a.target.AI) {
        a.target.addVisibleEnemy(a.actor);
      }
      if (typeof a.object == 'undefined') {
        //attack with bare hands -> melee attack without item
        var distance = Physics.calculateDistance(a.actor.currentX, a.actor.currentY,
                                                 a.target.currentX, a.target.currentY);
        if (distance > 1) {return}
        console.log('punch this sucker');
        var dice = window.random(1,100)*100;
        var hitchance = this.ChanceHandToHand(a.target);
        //console.log(hitchance);
        //console.log(dice);
        if (dice <= hitchance) {
          console.log('success');
          var impact = a.actor.iExpr / 2;
          //console.debug(impact);
          impact+=a.actor.stats.iStrength / 3;
          //console.debug(impact);
          impact+=  window.random(20,51)*100-25;
          //todo: object damage as modifier
          if (impact <0) impact = 0;
          //console.debug(impact);
          a.target.applyDamage({health:impact, iEP:impact*3, iAP:Points.AP_GET_HIT});
        } else {
          console.log('miss');
        }
        this.SubtractPoints(Points.AP_PUNCH, Points.EP_PUNCH);
      } else {
        switch (a.object.blueprint.itemClass ) {
        case 'weapon':
          //fire weapon
          if (a.object.getClipsize() < 1) {
            //try to reload, if weapon is out of ammo
            var ammo = a.actor.hasInventoryItem('ammo')
            if (typeof ammo != 'undefined' && ammo.clipsize > 0 && a.actor.currentAP >= (Points.AP_FIRE_GUN + Points.AP_RELOAD) ) {
              a.actor.reload({'actor':a.actor, 'ammo':ammo, 'item':a.object});
            }
          }
          if (a.object.getClipsize() > 0) {
            //face target
            var angle = Math.atan2(a.target.currentY - a.actor.currentY, a.target.currentX - a.actor.currentX);
            //convert atan2 (radians) to deg
            angle = angle/(Math.PI/180);
            angle = angle < 0 ? angle-=90 : angle+=90;
            var closest =  Points.Closest(Math.ceil(angle), [0,90,180,270,360]);
            console.debug(closest);
            if (closest === 360) closest = 0;
            if (a.actor.orientation != closest) {
              //actor needs to turn
              a.actor.orientation = closest;
              a.actor.SubtractPoints(Points.AP_TURN, 0);
            }
            //check if we are out of range
            var range = a.object.gunrange;
            var distance = Physics.calculateDistance(a.actor.currentX, a.actor.currentY, a.target.currentX, a.target.currentY);
            console.debug('range='+range+" distance: "+distance);
            if (distance > range) {
              window.TileEngine.addPopupText('out of range', this.getPositionInPx());
            } else {
              //decrease aps
              a.actor.SubtractPoints(Points.AP_FIRE_GUN, Points.EP_FIRE_GUN) ;
              //fire bullet
              window.TileEngine.spawnBullet(a);
            }
          } else {
            window.TileEngine.addPopupText('no ammo', {x:a.x, y:a.y});
          }
          break;
        case 'melee':
          var distance = Physics.calculateDistance(a.actor.currentX, a.actor.currentY, a.target.currentX, a.target.currentY);
          //abort the attack if we are too far away
          if (distance > 1) {return}
          var dice = window.random(1,100)*100;
          var hitchance = this.ChanceHandToHand(a.target);
          a.actor.SubtractPoints(Points.AP_USE_KNIFE, Points.EP_USE_KNIFE);
          console.log(hitchance);
          console.log(dice);
          if (dice <= hitchance) {
            console.log('success');
            
            var impact = a.actor.stats.iStrength / 20; 
            impact+= a.object.blueprint.damage;
            impact+=  window.random(20,51)*100-25;
            
            if (impact <0) impact = 0;
            //console.debug(impact);
            a.target.applyDamage({health:impact, iEP:impact*2, iEP: Points.GET_HIT, iAP: Points.AP_GET_HIT});
          } else {
            console.log('miss');
          }
          break;
        case 'throwing':
          console.log('will throw');
          //check if target is outside of max throwing distance
          var distance = Physics.calculateDistance(a.actor.currentX, a.actor.currentY, a.target.currentX, a.target.currentY); 
          var throw_strength = ( a.actor.stats.iStrength * 2 + 100 ) / 3;
          var throw_distance = 2 + ( throw_strength / Math.min( ( 3 + a.object.blueprint.weight / 3 ), 4 ) );
          throw_distance -= (throw_distance * (100 - a.actor.energy)) / 200;
          throw_distance = Math.floor(throw_distance/3);
          console.debug(throw_distance);
          var is_reachable = Physics.calcGetThroughChance(a.actor.currentX, a.actor.currentY, a.target.currentX, a.target.currentY);
          if (distance > throw_distance || is_reachable == 0) {
            window.TileEngine.addPopupText('out of range', {x:a.x, y:a.y});
          } else {
            //throw this thing
            var hitchance = (80 + a.actor.stats.iMarksmenship)/2;
            var dice = window.random(1,99)-20;
            if (dice <= hitchance) {
              //todo: affect objects near the impact
              console.log('success');
              window.TileEngine.addExplosion(a.target.currentX, a.target.currentY);
              a.target.applyDamage({health:30*(Math.random()+1), iEP:30, iAP: Points.GET_HIT});
              //knock target to the ground
              a.target.stance = 3;
            }
            console.debug(hitchance);
            //remove this thing from the inventory
            a.actor.removeItem(a.object);
            //todo: dynamic aps
            a.actor.SubtractPoints(Points.AP_THROW_ITEM, Points.EP_THROW_ITEM);
          }
          break;
        }
        
      }
    },
    dressWound: function(a) {
      if (a.target.bleeding == 0) return;
      var medkit = a.actor.hasInventoryItem('medkit');
      console.debug(medkit);
      if (typeof medkit == 'undefined' || medkit.condition == 0) return;
      //he's dead, jim
      if (a.target.health <= 0) return;
      var uiDressSkill = ( ( 7 * this.stats.iMedical ) +// medical knowledge
                           ( medkit.condition) + // state of medical kit
                           (10 * this.iExpr) // battle injury experience
                           )	/ 10;
      console.debug(uiDressSkill);
      var uiPossible = ( this.currentAP * uiDressSkill ) / 50;
      console.debug(uiPossible);
      var uiActual = uiPossible;
      var belowok, deficiency, medcost;
      var bRanOut = 0;
      if (a.target.health > this.OKLIFE) {
        console.log('he can make it');
        belowok = 0;
      } else {
        belowok = this.OKLIFE - a.target.health;
      }
      deficiency = (2* belowok);
      console.debug(deficiency);
      
      medcost = parseInt((uiActual +1) /2);
      console.debug(medcost);
      if (medcost > medkit.condition) {
        bRanOut = true;
      }
      
      var pointsLeft = uiActual;
      if ( pointsLeft > 0 && a.target.health < this.OKLIFE ) {
        //heal criticals first
        if ( pointsLeft >= (2 * belowok) ) {
          a.target.health = this.OKLIFE;
        } else {
          a.target.health += (pointsLeft / 2);
          pointsLeft = pointsLeft % 2;
        }
      }
      
      if (pointsLeft && a.target.bleeding > 0) {
        if (pointsLeft >= (a.target.stats.iHealth - a.target.health)) {
          pointsLeft -= (a.target.stats.iHealth - a.target.health);
          window.TileEngine.addPopupText('first aid completed', {x:a.x,y:a.y});
          a.target.bleeding = 0;
          //terminate completely
          a.actor.giveaid = '';
          a.target.receiveaid = '';
        } else {
          //bandage what we can
          a.target.bleeding -= pointsLeft;
          pointsLeft = 0;
          //continue this until we are done
          a.actor.giveaid = a;
          a.actor.receiveaid = a;
        }
      }
      
      if (medkit.condition - medcost < 0) {
        a.actor.removeItem(medkit);
        window.TileEngine.addPopupText('out of bandages', {x:a.x,y:a.y});
      }
      medkit.condition -= medcost;
      a.actor.subtractPoints(Points.AP_START_AID, Points.EP_FIRST_AID_START);
    },
    reload: function(n) {
      //reload and display a popup text
      n.item.reload(n.ammo);
      n.actor.removeItem(n.ammo);
      n.actor.SubtractPoints(Points.AP_RELOAD, Points.EP_RELOAD);
      window.TileEngine.addPopupText('reloads', this.getPositionInPx());
    },
    doreload: function() {
      //validity checks for ammo and weapon are performed in getRadialActions
      var ammo = this.hasInventoryItem('ammo');
      var weapon = this.hasInventoryItem('weapon');
      this.reload({'actor':this, 'ammo':ammo, 'item':weapon});
    },
    ChanceHandToHand: function(target) {
      if (target.collapsed) return 100;
      
      var attackRating = (3 * 60) +this.calcAgility() + this.stats.iStrength + (10 * this.iExpr);
      //console.log(attackRating);
      attackRating /= 6;
      //todo: fatigue checks
      //console.log(attackRating);
      attackRating -= ((100-this.energy)/3);
      if (this.bleeding) 
        attackRating-= (2* attackRating * this.stats.iHealth - this.health) / (3*this.stats.iHealth);
      //console.log(attackRating);
      
      //defender
      var defenderRating = (3 * this.calcAgility()) + 60 + target.stats.iStrength+ (10*target.iExpr);
      defenderRating /=6;
      defenderRating-=((100-target.energy)/3);
      if (target.bleeding) 
        defenderRating-= (2* defenderRating * target.stats.iHealth - target.health) / (3*target.stats.iHealth);
      if (target.stance ==2 ) defenderRating-= 20;
      console.log('defender' +defenderRating);
      console.log('attacker' +attackRating);
      
      chance = 67 + (attackRating - defenderRating ) / 3;
      //console.log('chance' +chance);
      
      return chance;
    },
    calcChanceHit: function(target, rweap) {
      var chance = 0;
      var item_condition = rweap.condition;
      var marksmanship = this.stats.iMarksmenship;
      if (item_condition >= marksmanship) {
        chance = marksmanship;
      } else {
        chance = (marksmanship + item_condition) /2;
      }
      //crouched firers get a bonus
      if (this.stance == 2) {
        chance*=1.2;
        //standing firers get a bonus
      } else if (this.stance == 1) {
        chance*=1.1;
      }

      var target_distance = Physics.calculateDistance(this.currentX, this.currentY, target.currentX, target.currentY);
      var gun_range = rweap.gunrange;
      if (target_distance > gun_range) {
        //penalize out of range targets
        //console.log('out of range');
        chance/=2;
      } else {
        //console.log('range: '+gun_range+' td '+target_distance);
        chance+=parseInt(gun_range-target_distance)*10;
      }
      if (target.stance == 1) {
        chance+=10;
      } else if (target.stance == 2) {
        chance-=5;
      } else if (target.stance == 3) {
        chance-=15;
      }
      if (chance > 100) chance = 100;
      if (chance < 0) chance = 0;
      return (chance/100);
    },
    SubtractPoints: function(iAP, iEnergy) {
      //only subtract aps in turn mode
      if ( window.TileEngine.mode == 'turnbased' && iAP > 0) {
        var newpoints = this.currentAP - iAP;
        if (newpoints < 0) newpoints = 0;
        this.currentAP = parseInt(newpoints);
      }
      //subtract energy points
      if ( iEnergy > 0 ) {
        var sEnergy = 100;
        sEnergy += (100 - this.energy);
        var bandage = this.stats.iHealth - this.health - this.bleeding;
        sEnergy+= 100 * (this.stats.iHealth - (this.health + (bandage / 2))) / this.stats.iHealth;
        //negative values are possible here
        
        if ( sEnergy > 0) {
          iEnergy = ((iEnergy * sEnergy) / 100);
        } else if (sEnergy < 0) {
          iEnergy = ((iEnergy * 100) / sEnergy);
        }
        //enforce min/max
        var newenergy = parseInt(this.energy -= iEnergy);
        if (newenergy > this.stats.sEnergy) newenergy = this.stats.sEnergy;
        if (newenergy < 0) newenergy = 0;
        this.energy = newenergy;
      }
      window.TileEngine.drawUI();
    },
    createInventory: function() {
      //this is a debug function
      //add default items
      var gun = window.TileEngine.itemlist[1].spawnNew();
      this.addItem(window.TileEngine.itemlist[5].spawnNew(), 'slot_body');
      this.addItem(gun,'slot_lhand');
      //0 -> knife
      this.addItem(window.TileEngine.itemlist[0].spawnNew());
      this.addItem(window.TileEngine.itemlist[10].spawnNew());
      this.addItem(window.TileEngine.itemlist[7].spawnNew());
      var ammo = window.TileEngine.itemlist[7].spawnNew();
      gun.reload(ammo);
      this.addItem(window.TileEngine.itemlist[3].spawnNew());
    },
    EditorProperties: function() {
      //orientation, health, faction, x, y
      //todo: min, max, dropdown for factions
      return [
        {"name":"id","type":"string","value":this.id},
        {"name":"orientation","type":"range","value":this.orientation, "max":360, "step":90, "min":0},
        {"name":"currentX","type":"integer","value":this.currentX},
        {"name":"currentY","type":"integer","value":this.currentY},
        {"name":"health","type":"range","value":this.health, "max":100, "min":1},
        {"name":"faction","type":"string","value":this.faction.name}
      ]
    }
  }
  return Actor;
});
