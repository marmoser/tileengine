/*
line of sight and visiblity related functions

*/
define(function() {
  return {
    findPath: function(start, dest) {
      //get the best path
      var map = window.TileEngine.getObjectComposite();
      var tiles = window.TileEngine.getTileMovementCosts();
      var layout = window.TileEngine.getObjLayout().mapLayout.map(function (arr) {return arr.slice();});
      /*
        Nodes -
        x: x position
        y: y position
        p: Index of the parent node (Stored in closed array)
        g: Cost from start to current node
        h: Heuristic cost from current node to destination
        f: Cost from start to desination going through current node
      */
      var Tilenode = function (x, y, p, g, h, f) {
        this.x = x;
        this.y = y;
        this.p = p;
        this.g = g;
        this.h = h;
        this.f = f;
      };
      
      var distance = function (current, end) {
        var x = current.x - end.x,
            y = current.y - end.y;
        var thistile = tiles[current.x][current.y];
        return (Math.pow(x, 2) + Math.pow(y, 2))*thistile.iAP;
      };
      
      var startnode = new Tilenode(start[0], start[1], -1, -1, -1, -1); // Start Node
      var endnode = new Tilenode(dest[0], dest[1], -1, -1, -1, -1); // End Node
      
      var cols = map.length-1, // Get number of rows from map
          rows = map[0].length-1, // Number of columns from map
          o = [], // Open Nodes
          c = [], // Closed Nodes
          mn = new Array(rows*cols), // Store open/closed nodes
          g = 0,
          h = distance(startnode, endnode),
          f = g + h;

      //adjust starting position if we are starting in a wallnode (no walking through walls)
      var start_in_wall = 0;
      var start_id = layout[startnode.x][startnode.y]-1;
      var start_properties = window.TileEngine.getObjLayout().settings.objectProperties[start_id];
      if (Number(layout[startnode.x][startnode.y]) > 1 && start_properties && ['wall','window'].indexOf(start_properties.type) > -1) {
        var angle = Math.atan2(endnode.y - startnode.y, endnode.x - startnode.x);
        angle = angle/(Math.PI/180);
        //angle = angle < 0 ? angle-=90 : angle+=90;
        //console.debug(angle);
        var walltype = start_properties.id.charAt(0);
        console.debug(' start in '+walltype);
        //adjust starting position if we start in a wall
        if ((walltype == 'l' || walltype == 'c') && Math.abs(angle < 180)) {
          startnode.x = startnode.x-1;
          start_in_wall = 1;
        } else if (walltype == 'r' ||  walltype == 'c' && angle > 0) {
          startnode.y = startnode.y-1;
          start_in_wall = 1;
        }
      }

      if (map[endnode.x][endnode.y] !== 0) {
        //find closest reachable tile
        var found = 0;
        var size = window.TileEngine.getMapSize()-1;
        var min_dist = Math.pow(size, 2) + Math.pow(size, 2);
        for (let pos of [{x: endnode.x+1, y:endnode.y},
                         {x: endnode.x-1, y:endnode.y},
                         {x: endnode.x, y:endnode.y+1},
                         {x: endnode.x, y:endnode.y-1}]) {
          if (pos.x < 0 || pos.y < 0 || map[pos.x][pos.y] !== 0) continue
          var tmpnode = new Tilenode(pos.x, pos.y,-1,-1,-1,-1);
          if (distance(startnode, tmpnode) < min_dist) {
            min_dist = distance(startnode,tmpnode);
            endnode = tmpnode;
            found = 1;
          }
        }
        if (!found) {
          console.log('unreachable, stop');
          return [];
        }
      }
      // Place start node onto list of open nodes
      o.push(startnode);
      
      while (o.length > 0) {

        // Locate Best Node
        var best = {
          c: o[0].f,
          n: 0
        };

        for (var i = 1, len = o.length; i < len; i++) {
          if (o[i].f < best.c) {
            best.c = o[i].f;
            best.n = i;
          }
        }

        // Set current to best
        var current  = o[best.n];

        // we've reached the end
        if (current.x === endnode.x && current.y === endnode.y) {
          var path = [{x: endnode.x, y: endnode.y}]; // Create Path 
          // Loop back through parents to complete the path
          while (current.p !== -1) {
            current = c[current.p];
            path.unshift({x: current.x, y: current.y});
          }
          //is the destination a wallnode?
          var dest_id = map[dest[0]][dest[1]]-1;
          var dest_properties = window.TileEngine.getObjLayout().settings.objectProperties[dest_id];
          //console.debug(dest_properties);
          if (dest_properties && ['wall','window'].indexOf(dest_properties.type) >-1) {
            //allow this movement depending on direction
            var lastnode = path.slice(-1)[0];
            var angle = Math.atan2(lastnode.y - dest[1], lastnode.x - dest[0]);
            angle = Math.abs(angle/(Math.PI/180));
            //can we walk up to the wall from this direction?
            var walltype = dest_properties.id.charAt(0);
            if ((walltype == 'l' || walltype == 'c') && angle == 180) {
              path.push({x: dest[0], y: dest[1]});
            } else if ((walltype == 'r' || walltype == 'c') && angle == 90) {
              path.push({x: dest[0], y: dest[1]});
            }
          }
          if (start_in_wall) {
            //startnode was modified, add the actual start node to the path
            path.unshift({x: start[0], y: start[1]});
          }
          //console.debug(path);
          return path;
        }

        // Remove current node from open list
        o.splice(best.n, 1);
        mn[current.x + current.y * rows * cols] = false; // Set bit to closed

        c.push(current);
        // Search new nodes in all directions
        for (var x = Math.max(0, current.x-1), lenx = Math.min(cols, current.x+1); x <= lenx; x++) {
          for (var y = Math.max(0, current.y-1), leny = Math.min(rows, current.y+1); y <= leny; y++) {
            // Check if location square is open
            if (Number(map[x][y]) === 0) {
              // Check if square is in closed list
              if (mn[x + y * rows * cols] === false) {
                continue;
              }
              // If square not in open list use it
              if (mn[x + y * rows * cols] !== true) {
                var n = new Tilenode(x, y, c.length-1, -1, -1, -1); // Create new node
                n.g = current.g + Math.floor(Math.sqrt(Math.pow(n.x - current.x, 2) + Math.pow(n.y-current.y, 2)));
                n.h = distance(n, endnode);
                n.f = n.g + n.h;
                o.push(n); // Push node onto open list
                mn[x + y * rows * cols] = true; // Set bit into open list
              }
            }
          }
        }
      }
      return [];
    },
    findPathPromise: function(start, dest) {
      //old async version, not useful for ai action calculation
      return new Promise (function (res,rej) {
        var pathfinder = {
          worker: new Worker("res/pathfindworker.js"),
          start: start,
          end: end,
          path: undefined,
          completed: false
        };
        
        // Event Listener
        pathfinder.worker.addEventListener('message', function(e) {
          pathfinder.path = e.data;
          pathfinder.completed = true;
          res(e.data);
        });
        
        pathfinder.worker.postMessage({start: start, end: end, map: window.TileEngine.getObjectComposite(), tileinfo: window.TileEngine.getTileMovementCosts()});
      });
    },
    findClosestLocation: function(sx,sy,dx,dy) {
      //find the closest location that is not occupied
      console.log('sx: '+sx+' sy: '+sy+' dx:'+dx+' dy:'+dy);
      var tiles = [];
      var d = 9999;
      var tile = {};
      var objmap = window.TileEngine.getObjLayout().mapLayout;
      var f1 = objmap[dx+1][dy];
      if (f1 == 0) tiles.push({"x":dx+1,"y":dy});
      var f2 = objmap[dx-1][dy];
      if (f2 == 0) tiles.push({"x":dx-1,"y":dy});
      var f3 = objmap[dx][dy+1];
      if (f3 == 0) tiles.push({"x":dx,"y":dy+1});
      var f4 = objmap[dx][dy-1];
      if (f4 == 0) tiles.push({"x":dx,"y":dy-1});
      for (entry of tiles) {
        var dist = this.calculateDistance(sx,sy,entry.x,entry.y);
        if (dist < d) {
          d = dist;
          tile = entry;
        }
      }
      return tile;
    },
    //get distance between two points
    calculateDistance: function (x1,y1,x2,y2) {
      return Math.sqrt( (x2-=x1)*x2+ (y2-=y1)*y2 );
    },
    getVisibleObjectsHelper: function(op, firstval, secondval, expand) {
      //returns array of visible tiles
      var tmp = [];
      var vis_distance = 7;
      var ops = {
        "+": function(a,b) {return a+b},
        "-": function(a,b) {return a-b}
      }; 
      var prop = window.TileEngine.gamedata.objectImagesLoaded.objectProperties;
      var objectmap = window.TileEngine.getObjLayout().mapLayout;
      //var stop_propagation = [];

      for (var i=1; i<=vis_distance; i++) {
        var f = ops[op](firstval,i);
        if (f <0) continue;
        for (var j=-i; j<=i;j++) {
          var s1 = secondval +j;
          if (s1 <0 || isNaN(s1)) continue;
          if (expand == "x") {
            if (!objectmap[s1]) {
              break;
            }
            var obj1 = objectmap[s1][f];
          } else {
            if (!objectmap[f]) {
              break;
            }
            var obj1 = objectmap[f][s1];
          }
          //var obj1 = (expand == "x" ? objectmap[s1][f] : objectmap[f][s1]);
          //the following fragment is buggy, commenting it means more work for calcGetThroughChance but better results
          /*if (obj1 > 0) {
            var obj_prop = prop[obj1-1];
            if (obj_prop && obj_prop.type === "wall") {
              //console.debug(f,s1);
              stop_propagation.push({f:f, s:s1});
            }
          }
          //stop searching behind solid objects
          var co = 0;
          for (v of stop_propagation){
            if (((op ==="-" && f <= v.f) || (op ==="+" && f >= v.f))
                && (s1 == v.s || s1+1 == v.s|| s1-1 == v.s)) co = 1;
          }
          if (co) continue;*/
          if (expand === "x" && this.calcGetThroughChance({x:secondval, y:firstval}, {x:s1, y:f}) > 0) 
            tmp.push({x:s1, y:f});
          else if (expand === "y" && this.calcGetThroughChance({x:firstval, y:secondval}, {x:f, y:s1}) > 0)
            tmp.push({x:f, y:s1});
        }
      }
      return tmp;
    },
    getVisibleObjects: function(a, show, recurse) {
      //what is currently in this actor's field of vision
      //determined by direction, weather, darkness, terrain, currently only by direction
      //flag visible items with visibility flag, visible non-pc actors will be rendered
      tmp = [];
      var l = window.TileEngine.getTileLayout().mapLayout;
      switch (a.orientation) {
      case 0:
        //-y, +-x
        tmp = this.getVisibleObjectsHelper("-", a.currentY, a.currentX, "x");
        break;
      case 90:
        //+x, +-y 
        tmp = this.getVisibleObjectsHelper("+", a.currentX, a.currentY, "y");
        break;
      case 180:
        //+y, +-x
        tmp = this.getVisibleObjectsHelper("+", a.currentY, a.currentX, "x");
        break;
      case 270:
        //-x, +-y 
        tmp = this.getVisibleObjectsHelper("-", a.currentX, a.currentY, "y")
        break;
      }
      var color = a.AI ? 19 : 23; //Math.floor(Math.random() * (25 - 15) + 15);
      a.visibleObjects.actors = [];
      for (let i of tmp) {
        //paint visible area in a different color
        if (show == "1") {l[i.x][i.y] = color;}
        if (!window.TileEngine.objectMap[i.x][i.y]) continue;
        for (var obj of window.TileEngine.objectMap[i.x][i.y]) {
          //only see actors of different faction
          if (obj.constructor.name === 'Actor'
              && obj.faction != a.faction
              && !obj.destroyed) {
            a.addVisibleEnemy({actor: obj, lastX:obj.currentX.valueOf(), lastY:obj.currentY.valueOf()});
            if (obj.AI && recurse) {
              console.log('will recurse for '+obj.id);
              this.getVisibleObjects(obj, 0, 0);
            }
          }
          /*if (obj.discovered === false) {
            console.debug(obj);
            obj.discovered = true;
            }*/
        }
      }
    },
    calcGetThroughChance: function(start, dest) {
      //todo: actor.js calcChanceHit does roughly the same ( calculates if we can hit the target
      //with the current weapon or is it out of range )
      //can we hit the target at dest starting at start?
      var deltaX = dest.x - start.x;
      var deltaY = dest.y - start.y;
      var distance = this.calculateDistance(start.x,start.y,dest.x,dest.y);
      if (distance <=0 ) return 0;
      var hitchance = 1;
      var prop = window.TileEngine.gamedata.objectImagesLoaded.objectProperties;
      var objectmap = window.TileEngine.getObjLayout().mapLayout;
      for (let x = 1; x<=Math.floor(distance); x++) {
        var xtile = Math.round(start.x + ((dest.x - start.x) * (x / distance)));
        var ytile = Math.round(start.y + ((dest.y - start.y) * (x / distance)));
        //if (deltaY > 0 && ytile > dest.y||
        //deltaX > 0 && xtile > dest.x) break;
        //now check if something is there
        var current_obj = objectmap[xtile][ytile];
        if (current_obj > 0) {
          var obj_prop = prop[current_obj];
          if (obj_prop) {
//            console.log('x'+xtile+'y'+ytile);
//            console.debug(obj_prop);
            if (obj_prop.type === 'wall') {
//              console.log('hit a wall');
              return 0;
            }
            hitchance-=obj_prop.cover;
//            console.log(hitchance);
            //todo: other materials reduce or destroy hit chance
          }
        }
      }
      return hitchance;
    },
    findBestCover: function(a, searchradius, shooter) {
      //find the tile inside searchradius that offers the best cover against being shot at
      //x+5, x-5; y+5, y-5
      var startx = a.currentX;
      var starty = a.currentY;
      var mapsize = window.TileEngine.getMapSize();
      var maxchance = 1;
      var bestcover = {
        x: '',
        y: ''
      };
      for (var i=-searchradius; i<=searchradius; i++) {
        var xpos = startx + searchradius;
        if (xpos <0) xpos = 0;
        if (xpos > mapsize) xpos = mapsize;
        
        var ypos = starty + searchradius;
        if (ypos <0) ypos = 0;
        if (ypos >0) ypos = mapsize;
        //now we can calc the best cover chance for all remaining tiles
        var currentchance = this.calcGetThroughChance({x:shooter.currentX,y:shooter.currentY},{x:xpos, y:ypos});
        //console.log(currentchance);
        if (currentchance < maxchance) {
          maxchance = currentchance;
          bestcover.x = xpos;
          bestcover.y = ypos;
        }
      }
      return bestcover;
    },
    movementCosts: function(path) {
      //calculates the cost of moving along path
      var costmap = window.TileEngine.getTileMovementCosts();
      var cost = 0;
	    for (var i=1; i<path.length; i++) {
        //skip first tile, since it is the current tile
        var tile = path[i];
        cost+= costmap[tile.x][tile.y].iAP;
	    }
      return cost;
    }
  }
});
