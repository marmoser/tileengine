/*
input handler
*/ 

define([
  '../res/Actor',
  '../res/Physics',
  '../res/Door',
  '../res/Container'
],
       function(Actor, Physics, Door, Container) {
    //callback for mouse actions
  var mousecallbackEditor = function(coords) {
    if (coords.which === 1) {
      //left mouse button: paint
      var get_tile = window.TileEngine.getXYCoords(coords.x,coords.y);
      if (window.actorMode) {
        //cancel if an object already exists at this point
        if (window.TileEngine.tileField[1].mapLayout[get_tile.x][get_tile.y] > 0) {return}
        if (window.selectedTile == 'Actor') {
          var a = new Actor('player', get_tile.x, get_tile.y, 0, window.TileEngine.playerfaction, null);
          window.TileEngine.actors.push(a);
        } else {
          //object
          var selected = window.selectedTile;
          if (selected.type == 'door') {
            var new_obj  = new Door({"state":selected.state, "model":selected.id});
          } else if (selected.type == 'container') {
            var new_obj = new Container(false, selected.id);
          }
          window.TileEngine.objectMap[get_tile.x][get_tile.y] = [];
          window.TileEngine.objectMap[get_tile.x][get_tile.y].push(new_obj);
        }
      } else if (!window.objectMode) {
        //window.TileEngine.tileField[0].setTile(get_tile.x, get_tile.y, window.selectedTile.x);
        window.TileEngine.tileField[0].mapLayout[get_tile.x][get_tile.y] = window.selectedTile.x;
      } else {
        //window.TileEngine.tileField[1].setTile(get_tile.x, get_tile.y, window.selectedTile.x+1);
        var interactiveObject = window.TileEngine.isInteractiveObjects(get_tile.x,get_tile.y);
        var inserttile = window.selectedTile.x;
        if (inserttile >0) inserttile++;
        window.TileEngine.tileField[1].mapLayout[get_tile.x][get_tile.y] = inserttile;
        if (interactiveObject.length > 0) {
          window.TileEngine.objectMap[get_tile.x][get_tile.y] = inserttile;
        }
      }
    } else if (coords.which === 3) {
      //right mouse button: open details
      var get_tile = window.TileEngine.getXYCoords(coords.x,coords.y);
      var interactiveObjects = window.TileEngine.isInteractiveObjects(get_tile.x,get_tile.y);
      if (interactiveObjects.length > 0) {
        var interactiveObject = interactiveObjects[0];
        var htmlinput = window.tactical.propertiesToHtml(interactiveObject.EditorProperties());
        $("#elementdescriptor").empty();
        $("#elementdetails_form").html(htmlinput).append(
          $("<button>", {type: "submit", html:"commit"})
        ).on("click", function() {
          var actor = $(this).data("object");
          if (typeof actor == "undefined") return false;
          for (v of $(":input", this)) {
            var nname = v.name;
            var datatype = v.getAttribute("data-type");
            if (datatype == "integer" && v.value == parseInt(v.value))
              actor[nname] = parseInt(v.value);
            else if (v.name == "faction") {
              //make sure .faction points to the right object, it is not a string
              actor.faction = window.TileEngine[v.value+'faction'];
            } else if (datatype == "string") {
              actor[nname] = v.value;
            }
          }
          return false;
        }).data("object", interactiveObject);
        //delete interactive object
        $("#elementdetails_form").append(
          $("<button>", {type: "submit", html:"delete"})
            .on("click", function() {
              var actor = $("#elementdetails_form").data("object");
              if (actor.constructor.name == 'Actor') {
                window.TileEngine.actors.splice(window.TileEngine.actors.indexOf(actor),1);
              } else {
                window.TileEngine.objectMap[get_tile.x][get_tile.y] = [];
              }
            })
        );
        //containers and actors get an inventory button
        var objclass = interactiveObject.constructor.name;
        if (objclass == "Actor" || objclass == "Container") {
          $("#elementdetails_form").append(
            $("<button>", {type: "button", html:"open inventory popup"})
              .on("click", function() {
                var actor = $("#elementdetails_form").data("object");
                window.TileEngine.drawInventoryPopover(actor, coords.x, coords.y);
                var items = [];
                //draw all items for selection
                $("#elementselector").empty();
                for (i in window.TileEngine.itemlist) {
                  items.push({obj: window.TileEngine.itemlist[i].spawnNew()});
                }
                window.TileEngine.drawInventoryPopover({
                  inventory: items,
                  id: 'Available items'
                }, coords.x-100,coords.y-150);
              })
          );
        }
      }
    } else if (coords.which === 2) {
      window.TileEngine.exportCurrentMap();
    }
  }

  var mousecallback = function(coords) {
    //we have to check: is an object there? ->open interaction menu
    //is a controllable actor there? -> rightclick -> interaction menu,
    //leftclick -> take control or perform default action
    //get tile by coordinates
    //todo: deobfuscate
    var get_tile = window.TileEngine.getXYCoords(coords.x,coords.y);
    var interactiveObjects = window.TileEngine.isInteractiveObjects(get_tile.x,get_tile.y);
    var selected = window.TileEngine.selected;
    for (var actor of selected) {
      if (actor.inventoryOpen) {return}
    }
    var selectaction = 0;
    if (interactiveObjects.length > 0) {
      var interactiveObject = interactiveObjects[0];
      //clicks on Actors
      if (interactiveObject instanceof Actor) {
        switch (coords.which) {
        case 1:
          //left click: toggle selection
          if (interactiveObject.faction.name  == 'player') {
            selectaction = 1;
            if (window.TileEngine.selected.indexOf(interactiveObject) == -1) {
              //unselect existing selections
              for (var actor of window.TileEngine.selected) {
                actor.is_selected = 0;
              }
              window.TileEngine.selected = [];
              window.TileEngine.selected.push(interactiveObject);
              //console.log('will select');
              interactiveObject.is_selected = 1;
            } else {
              window.TileEngine.selected.splice(window.TileEngine.selected.indexOf(interactiveObject),1);
              interactiveObject.is_selected = 0;
              //console.log('will unselect');
            }
          }
          break;
        }
      }
      if (!selectaction) {
        if (selected.length) {
          //add TileObject, since look/move should always be possible unless the distance is 0
          if (!(selected[0].currentX == get_tile.x && selected[0].currentY == get_tile.y)) {
            interactiveObjects.push(window.TileEngine.TileObject);
          }
          window.TileEngine.spawnRadialMenu(interactiveObjects, coords, selected[0]);
        } else {
          window.TileEngine.addPopupText('no actor selected', {x:coords.x, y:coords.y});
        }
      }
    } else {
      //clicked on a tile
      //draw popup to confirm movement
      if (window.TileEngine.selected.length > 0) {
        var interactiveObjects = new Array(window.TileEngine.TileObject);
        window.TileEngine.spawnRadialMenu(interactiveObjects, coords, selected[0]);
      }
      //move selected to x,y
      /*      window.TileEngine.selected.map(function(actor) {
      //actor.move(get_tile.x, get_tile.y);
      actor.actionqueue.push({'action': 'moveto', 'x': get_tile.x, 'y': get_tile.y});
      actor.currentAction = actor.performNextAction();
      });*/
    }
    redrawui = 1;
  }

var keyupcallback = function(code,keyup,ev) {
  switch(ev.key) {
    case "u": //
      this.keydown = 0;
      if (this.tmpmap) 
        window.TileEngine.tileField[0].mapLayout = this.tmpmap;
      break;
  }
}

var keyboardcallback = function(code, keydown, ev ) {
    switch(ev.key) {
      case "ArrowLeft": //left
          window.TileEngine.scroll("left");
          window.TileEngine.scroll("down");
          //window.TileEngine.move("left");
        break;
      case "ArrowRight": //right
          window.TileEngine.scroll("right");
          window.TileEngine.scroll("up");
          //window.TileEngine.move("right",100);
        break;
      case "ArrowDown": //down
         window.TileEngine.scroll("down");
         window.TileEngine.scroll("right");
         // window.TileEngine.move("up");
        break;
      case "ArrowUp": //up
          window.TileEngine.scroll("up");
          window.TileEngine.scroll("left");
          //window.TileEngine.move("down");
        break;
      case "PageUp": //pg up
        window.TileEngine.move("down", 500);
        break;
      case "PageDown": //pg down
        window.TileEngine.move("right", 500);
        break;
      case "a":
        window.TileEngine.tileField[0].setZoom("in");
        window.TileEngine.tileField[1].setZoom("in");
        break;
      case "s":
        window.TileEngine.tileField[0].setZoom("out");
        window.TileEngine.tileField[1].setZoom("out");
        break;
      case "i": //i -> open inventory
        if (window.TileEngine.selected.length == 1){
          let a = window.TileEngine.selected[0];
          if (!a.inventoryOpen) {
            a.openCharacterInventory(a.currentX,a.currentY);
          } else {
            //close callback if inventory is currently open
            $("a[data-toggle-role='close']").click();
          }
        } else {
          window.TileEngine.addPopupText('no actor selected');
        }
        break;
      case "u": //show visible tiles
        if (this.keydown) {return}
        this.keydown = 1;
        //ensure we get a separate object here
        this.tmpmap = JSON.parse(JSON.stringify(window.TileEngine.getTileLayout().mapLayout));
        for ( act of window.TileEngine.actors ) {
          Physics.getVisibleObjects(act,1);
        }
        break;
      case "e": //end turn
        window.TileEngine.endTurn();
      break;
    }
    //drawThings();
}

  //constructor
  function InputHandler(document, ctx, type) {
    this.document = document;
    //CanvasRenderingContext2D
    this.canvas = ctx;
    //canvas element
    this.context = ctx.canvas
    if (type === 'editor') {
      this.mouse_action(mousecallbackEditor);
    } else {
      this.mouse_action(mousecallback);
    }
    this.keyboard_action(keyboardcallback);
    this.keyboard_action_up(keyupcallback);
  }
  
  InputHandler.prototype = {
    //handle mouse down
    mouse_action: function(callback) {
        var that = this;
        this.context.addEventListener('mousedown', function(ev) {
          ev.preventDefault();
          var coords = {};
          coords.x = ev.pageX - that.context.offsetLeft;
          coords.y = ev.pageY - that.context.offsetTop;
          coords.which = ev.which;
          coords.e = ev;
          callback(coords);
        }, false);
    },
    keyboard_action: function(callback) {
      this.document.onkeydown = function(ev) {
        var keycode = ev.keyCode;
        var pressed = true;
        callback(keycode, pressed, ev);
      };
    },
    keyboard_action_up: function(callback) {
      this.document.onkeyup = function(ev) {
        var keycode = ev.keyCode;
        var pressed = true;
        callback(keycode, pressed, ev);
      };
    }
  }
  
  return InputHandler;
  
});
