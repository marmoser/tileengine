/*
  item class
  item definitions should be loaded from json

  iteminstance is a instance of the item
*/

define(function() {

    function ItemInstance(item, condition) {
	this.blueprint = item;
	this.condition = condition == 'undefined' ? condition : 100;
	this.attachments = new Array();
	this.isReady = false;
	this.weight = this.calculateWeight();
	//prepare attachment slots
	if (item.attachments != 'undefined') {
            for (var x in item.attachments) {
		this.attachments.push( {'type':item.attachments[x], 'item': 'undefined'} );
            }
	}
	if (item.clipsize != 'undefined') {
            //this -> current, item-> max
            this.clipsize = item.clipsize;
	}
	switch (this.blueprint.itemClass) {
        case 'weapon' :
            this.gunrange = 6;
	}
    }

    ItemInstance.prototype = {
      constructor: ItemInstance,
      generateQuickinfo: function () {
        //show most important stats for this item
        var returnstring = 
        [
        this.blueprint.name,
        "Condition:" +this.condition,
        "Weight:" +this.calculateWeight()
        ];
        
        switch (this.blueprint.itemClass) {
          case 'armor':
            returnstring.push("Protection: "+this.blueprint.protection);
          break;
          case 'weapon':
            returnstring.push("Ammo: " +this.blueprint.ammotype);
            returnstring.push("Clip: " +this.getClipsize());
          break;
          case 'ammo': 
            returnstring.push("Clip: " +this.clipsize);
          break;
        }
        
        return returnstring.join("<br>");
      },
      generateDetails: function() {
        //item details popup
        var row =   $("<div>", {class: "inventory-row"});
        
        row.append("<img src='"+this.blueprint.image.src+"'>");
        row.append("<p>"+this.blueprint.description+"</p>");
        //string += this.blueprint.description;
        //1)special info for weapons : aps needed to attack, damage
        //...
        //2)possible and actual attachments
        console.debug(this.attachments);
        for (var x in this.attachments) {
          //existing items
          if (this.attachments[x].item != 'undefined') {
            //this slot is not empty
            row.append(
              $("<div>", {class: "inventory-cell inventory-slot_generic"}).append(
                $("<div>", {class: "inventory-item"}).css({
                 "background-image": "url('" + this.attachments[x].item.blueprint.image.src + "')",
                   "background-size" : "contain",
                   "background-repeat": "no-repeat",
                   "background-position": "center"
                })
                .addClass(this.attachments[x].item.blueprint.name)
                .data('content', this.attachments[x].item)
                .append(
                //quickinfo popover
                $("<div>", {class: "inv-popover"}).append(
                  $("<p>", {html: this.attachments[x].item.generateQuickinfo()})
                  )
                )
                .hover(function(i) {
                  //show/hide description popover
                  $(".inv-popover").hide();
                  $(this).children(":first").toggle();
                }, function(i) {
                  $(this).children(":first").hide();
                })
              ).data('slot', 'slot_'+this.attachments[x].type)
                .sortable(sortable)
            );
          } else {
            row.append(
              $("<div>", {class: "inventory-cell", title: this.attachments[x].type}).append(
                $("<div>", {class: "inventory-item"}).css(
                "background-image", "none"
                )
              ).data('slot', 'slot_'+this.attachments[x].type)
              .sortable(sortable)
            )
          }
        }
        return row;
      },
      getClipsize: function() {
        //how many bullets are left?
        var clipsize = 0;
        for (var i in this.attachments) {
          if (this.attachments[i].type == 'ammo' && typeof this.attachments[i].item != 'string') {
            return this.attachments[i].item.clipsize;
          }
        }
        return clipsize;
      },
      decreaseAmmo: function(n) {
        var currentload = this.getClipsize();
        if (currentload - n < 0) {
          currentload = 0;
        } else {
          currentload-=n;
        }
        for (var i in this.attachments) {
          if (this.attachments[i].type == 'ammo' && 
            typeof this.attachments[i].item != 'string') {
            if (currentload > 0) {
              this.clipsize = currentload;
              this.attachments[i].item.clipsize = currentload;
            } else {
              this.clipsize = 0;
              this.attachments[i].item = 'undefined';
            }
          }
        }
      },
      //use clip to reload
      reload: function(item) {
        for (var i in this.attachments) {
          if (this.attachments[i].type == 'ammo') {
            this.attachments[i].item = item;
          }
        }
      },
      //blueprint weight + attachments
      calculateWeight: function() {
        var weight = this.blueprint.weight;
        for (var i in this.attachments) {
          if (typeof this.attachments[i].item != 'string') {
            weight += this.attachments[i].item.blueprint.weight;
          }
        }
        return weight;
      }
    }
    //constructor
    function Item(i, spritelist) {
      //blueprints are called with objects as arguments
      if (typeof i == 'object') {
        this.itemClass = i.itemClass,
        this.name= i.name,
        this.index = i.index,
        this.size = i.size,
        this.weight = i.weight;
        this.image = spritelist[i.index];
        this.slot = i.slot;
        this.attachments = i.allowed_attachments;
        this.description = i.description;
        //additional class based attributes
        switch (this.itemClass) {
          case 'armor' :
            this.protection = i.protection;
            this.degradation = i.degradation;
            break;
          case 'weapon' :
            this.ammotype = i.ammotype;
            this.damage = i.damage;
            break;
          case 'ammo' :
            this.clipsize = i.clipsize;
            break;
          case 'melee':
            this.damage = i.damage;
            break;
        }
      } else {
        console.log('invalid item');
      }
    }

    //prototype
    Item.prototype = {
      constructor: Item,
      spawnNew: function() {
        //create a new item instance
          return new ItemInstance(this);
      }
    };

    return Item;
  }
);
