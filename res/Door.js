/*
  Door.js
  Doors are objects, blocking two sets of space if closed.
  
  functions:
  -affect visibility of area behind this door
  -affect pathfinding
  -can be trapped
  -can be destroyed (resistance)
  -can be locked (and lockpicked)
  -actions: open/close, search for traps, force, pick lock
*/

define([
  '../res/Points'
  ],
function(Points) {
  //constructor
  function Door(args) {
    this.class = "Door";
    this.orientation = args['orientation'];
    if (typeof this.orientation == 'undefined') this.orientation = 0;
    this.state = args['state'];
    this.model = args['model'];
    this.is_locked = args['is_locked'];
    this.is_lock_known = 0;
    this.is_trapped = args['is_trapped'];
    this.is_trap_known = 0;
    this.resistance = args['resistance'];
    this.modelIndex = this.getModelIndex(this.model);
  }

  Door.prototype = {
    constructor: Door,
    getRadialActions: function(actor) {
      var returnvalue = {};
      
      if (this.state === 'open') {
        returnvalue["close"] = "close";
      }
      if (this.state === 'closed') returnvalue["open"] = "open";
      
      if (this.is_locked && this.is_lock_known) {
        returnvalue["force"] = "force";

        if ( actor.hasInventoryItem('crowbar') ) {
          returnvalue["crowbar"] = 'crowbar';
        }

        if ( actor.hasInventoryItem('lockpick') ) {
          returnvalue["pick lock"] = 'picklock';
        }
      }

      if (this.is_trapped && this.is_trap_known) {
         returnvalue["disarm trap"] = 'disarm';
      }
      
      if (!this.is_trap_known) {
        returnvalue["detect traps"] = 'trapsearch';
      }
      return returnvalue;
    },
    getModelIndex: function(id) {
      var objectProperties = window.TileEngine.layers[1].objectProperties;
      for (var i in objectProperties) {
        if (objectProperties[i].id == id) {return i}
      }
      return;
    },
    drawImage: function(tileX,tileY) {
      if (tileX && tileY) {
        window.TileEngine.tileField[0].drawShadow(tileX,tileY,'rgba(255, 255, 0, 0.2)');
      }
      //image depends on state
      var sprites = window.TileEngine.gamedata.objectImagesLoaded;
      return sprites.files[this.modelIndex];
    },
    //action functions
    close: function(a) {
      console.log("close");
      this.state = 'closed';
      var type = this.model.split('_');
      this.model = type[0]+'_closed';
      this.modelIndex = this.getModelIndex(this.model);
      //use open door aps/eps
      a.iAP = Points.AP_OPEN;
      a.iEP = Points.EP_OPEN_DOOR;
    },
    open: function(a) {
      if (this.is_locked) {
        window.TileEngine.addPopupText('locked', {x:a.x, y:a.y});
        this.is_lock_known = 1;
        return;
      }
      this.state = 'open';
      var type = this.model.split('_');
      this.model = type[0]+'_open';
      this.modelIndex = this.getModelIndex(this.model);
      a.iAP = Points.AP_OPEN;
      a.iEP = Points.EP_OPEN_DOOR;
    },
    force: function(a) {
      console.debug( a.actor.stats.iStrength);
      //todo: skillchecks
      if ( a.actor.stats.iStrength > this.resistance ) {
        this.is_locked = 0;
        this.state = 'open';
        window.TileEngine.addPopupText('success', {x:a.x, y:a.y});
      }
      a.iAP = Points.AP_FORCE_DOOR;
      a.iEP = Points.EP_FORCE_DOOR;
    },
    trapsearch: function(a) {
      if (this.is_trapped) {
        this.is_trap_known = 1;
      } else {
        window.TileEngine.addPopupText('no traps detected', {x:a.x, y:a.y});
      }
      a.iAP = Points.AP_EXAMINE_DOOR;
      a.iEP = Points.EP_EXAMINE_DOOR;
    },
    disarm: function(a) {
      //todo
      a.iAP = Points.AP_UNTRAP;
      a.iEP = Points.EP_UNTRAP;
    },
    picklock: function(a) {
      if ( a.actor.stats.iStrength > this.resistance ) {
        this.is_locked = 0;
        window.TileEngine.addPopupText('success', {x:a.x, y:a.y});
      }
      a.iAP = Points.AP_PICKLOCK;
      a.iEP = Points.EP_PICKLOCK;
    },
    crowbar: function(a) {
      //todo
      a.iAP = Points.AP_CROWBAR;
      a.iEP = Points.EP_CROWBAR;
    },
    applyDamage: function(a) {
      this.destroyed = 1;
      //todo: different reactions depend on materials
    },
    EditorProperties: function(a) {
      return [
        {"name":"state","type":"string","value":this.state},
        {"name":"orientation","type":"integer","value":this.orientation},
        {"name":"model","type":"string","value":this.model},
        {"name":"is_locked","type":"integer","value":this.is_locked},
	      {"name":"is_trapped","type":"integer","value":this.is_trapped},
        {"name":"resistance","type":"integer","value":this.resistance}
      ]
    }
  }
  return Door;
});
